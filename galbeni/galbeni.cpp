/*
După ce au descoperit ascunzătoarea piratului Spânu, marinarii de pe corabia “Speranţa” au hotărât să ofere sătenilor o parte din comoara acestuia. 
Întrucât comoara avea un număr nelimitat de bani din aur, numiţi galbeni, singura problemă a marinarilor a fost regula după care să împartă banii. 
După îndelungi discuţii au procedat astfel: i-au rugat pe săteni să se aşeze în ordine la coadă şi să vină, pe rând, unul câte unul pentru a-şi ridica galbenii cuveniţi. 
Primul sătean a fost rugat să îşi aleagă numărul de galbeni, cu condiţia ca acest număr să fie format din exact K cifre. Al doilea sătean va primi un număr de galbeni 
calculat astfel: se înmulţeşte numărul de galbeni ai primului sătean cu toate cifrele nenule ale acelui număr, rezultatul se înmulţeşte cu 8 şi apoi se împarte la 9 păstrându-se 
doar ultimele K cifre ale câtului împărţirii. Dacă numărul obţinut are mai puţin de K cifre, atunci acestuia i se adaugă la final cifra 9, până când se completează K cifre. 
Pentru a stabili câţi galbeni primeşte al treilea sătean, se aplică aceeaşi regulă, dar pornind de la numărul de galbeni ai celui de-al doilea sătean. Regula se aplică în 
continuare fiecărui sătean, plecând de la numărul de galbeni primiţi de săteanul care a stat la coadă exact în faţa lui.

Cerinţă
Cunoscând numărul de galbeni aleşi de primul sătean, determinaţi numărul de galbeni pe care îl va primi al N-lea sătean.

Date de intrare
Fişierul galbeni.in conţine pe prima linie cele 3 numere naturale nenule S K N separate prin câte un spaţiu, unde S reprezintă numărul de galbeni ales de primul sătean, K 
este numărul de cifre ale numărului S, iar N reprezintă numărul de ordine al săteanului pentru care se cere să determinaţi numărul de galbeni primiţi.

Date de ieşire
Fişierului galbeni.out conţine pe unica sa linie un număr natural reprezentând rezultatul determinat.

Restricţii
• 2 ≤ N ≤ 1 000 000 000
• 1 ≤ K ≤ 3
• Se garantează că S are exact K cifre.
*/
#include <stdio.h>
#include <math.h>

 int n, k, s;

int FormNumar(int );
 int Produs(int );
void Solve();

int viz[1000], el[1000];

int main()
{
    freopen("galbeni.in", "rt", stdin);
    freopen("galbeni.out", "wt", stdout);

    scanf("%d %d %d", &s, &k, &n);
    Solve();

    return 0;
}

void Solve()
{
      int i,  x = 0, p;

      viz[s] = 1;
      el[1] = s;
      for(i = 2; i <= n; i++)
        {
            s = Produs(s);
            s = FormNumar(s);

            if(viz[s])
                {
                    x = n-(viz[s]-1);
                    x = x%(i-viz[s]);
                    if(!x)
                        {
                            printf("%d\n", el[i-1]);
                        }
                    else
                        {
                            printf("%d\n", el[viz[s]+x-1]);
                        }
                    return ;
                }
            viz[s] = i;
            el[i] = s;
        }
    printf("%d\n", el[n]);
    return ;
}

int Produs(int x)
{
     int k = 0, s = x;

    while(x > 0)
        {
            k = x%10;
            if(k)
                s *= k;
             x /= 10;
        }
        s = (s*8)/9;
        return s;
}

int FormNumar(int x)
{
    int  p = 0;
    p = pow(10, k);
    x = x%p;
    while(x*10 < p)
        {
            x = x*10 + 9;
        }

    return x;
}

	
/*
Şirul de numere Fibonacci este definit în felul următor: primele două elemente ale şirului sunt egale cu 1, 
fiecare element următor este suma celor două elemente care îl precedă.
Şirul de numere Fibonacci este scris fără spaţii, astfel încât începutul lui arată în felul următor:
11235813213455...

Cerinţă
Se cere să scrieţi un program care să determine cifra de pe poziţia N din şirul obţinut (poziţiile în şir 
sunt numerotate începând cu 1).

Date de intrare

Fişierul de intrare fib.in va conţine un singur număr N – poziţia cifrei.

Date de ieşire

Fişierul de ieşire fib.out va conţine o singură linie pe care va fi afişat un număr natural – valoarea cifrei de pe poziţia N.

Restricţii

    1 ≤ N ≤ 10000000

*/
 
#include <stdio.h>
#include <string.h>
#include <memory.h>

#define LG_MAX 3500 + 1

typedef int BigNumber[LG_MAX];



BigNumber a, b, c;

long int n;




void afisare(BigNumber x);
void suma(BigNumber ,BigNumber ,BigNumber );
void diferenta(BigNumber a,BigNumber b,BigNumber d);
void fibonaci2(long int );

void add(int A[], int B[]);
void sub(int A[], int B[]) ;




int main()
{

    freopen("fib.in", "rt", stdin);
    freopen("fib.out", "wt", stdout);

    scanf("%ld", &n);

    fibonaci2(n);

    return 0;
}

void fibonaci2(long int n){

		long long int k = 1;
		 a[0] = 1;
		 a[1] = 1;
		 b[1] = 0;
		 b[0] = 0;
		 c[0] = 0;

	if(n == 1)printf("1");

	else
        {

		while(k < n)
        {



			 	suma(a, c, b);

			 	memmove(c, a, sizeof(a));
			 	memmove(a, b, sizeof(b));
			 	k += b[0];
			 	//afisare(b);
			 	///printf("\n%d\n", k);
		 }
int t = b[0] - 1;
		 while(k != n){k--;t--;}

//afisare(b);
//afisare(b);
		 printf("%d\n",b[b[0] - t]);

        }
}



void suma(BigNumber a,BigNumber b,BigNumber s)
{
    int i,cifra,t = 0,max;

    //completam numarul cel mai mic cu zeroouri nesemnificative
    if(a[0] < b[0]) { max = b[0]; //for(i = a[0]+1; i <= b[0]; i++) a[i] = 0;
                                    memset(a+a[0]+1,0,b[0]*sizeof(int)); }
    else            { max = a[0]; //for(i = b[0]+1; i <= a[0]; i++) b[i] = 0;
                                    memset(b+b[0]+1,0,a[0]*sizeof(int)); }

    for(i = 1; i <= max; i++)
    {
      cifra = a[i] + b[i] + t; //calculam noua cifra
      s[i] = cifra % 10;
      t = cifra/10;            //calculam cifra de transport
    }
    if(t) s[i] = t; else i--;
    s[0] = i;
}

void afisare(BigNumber x)
{
   int t = 0;
     for(int i = x[0]; i >= 1; i--)
                if(x[i])
                {
                   t = i;  break;
                }
    for(int i = t; i >= 1; i--)
            printf("%d",x[i]);


}

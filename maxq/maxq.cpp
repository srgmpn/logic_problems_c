/*
Johnie a început să se joace cu un vector de numere. El dispune iniţial de un vector V cu N numere întregi 
(numerotate de la 0 la N-1) şi poate efectua următoarele operaţii:
- schimbarea elementului de pe poziţia p cu un alt număr întreg;
- aflarea subsecvenţei de sumă maximă din V inclusă între indicii a şi b;

Cerinţă
Ajutaţi-l pe Johnie să efectueze repede operaţiile de mai sus.

Date de intrare
Fişierul de intrare maxq.in conţine pe prima linie numărul N reprezentând dimensiunea vectorului. 
Pe următoarea linie se găsesc N elemente reprezentând valorile iniţiale ale vectorului. 
Urmatoarea linie conţine M, reprezentând numărul de operaţii. Pe fiecare dintre următoarele M linii sunt descrise cele M operaţii în forma următoare:
- “0 i p”: numărul 0 de la început codifică faptul că operaţia curentă este una de schimbare; astfel elementul de pe poziţia i a vectorului se înlocuieşte cu numarul întreg p;
- “1 a b”: numărul 1 de la început codifică faptul că operaţia curentă este o întrebare; astfel se cere să se afle subsecvenţa de sumă maximă din vector inclusă între indicii a şi b (a≤b);

Date de ieşire
Fişierul de ieşire maxq.out trebuie să conţină un număr de linii egal cu numărul de întrebări din fişierul de intrare. 
Pe linia i se cere să se afişeze un singur număr reprezentând suma maximă ce se poate obţine în contextul întrebării i din fişierul de intrare (i=1,2,...); 
în cazul în care vor exista doar subsecvenţe de sumă negativă se va afişa 0.

Restricţii
1 ≤ N ≤ 200000;
1 ≤ M ≤ 200000;
toate elementele vectorului aparţin intervalului [-100000, 100000];
definim o subsecvenţă ca fiind un şir de termeni consecutivi din vector, iar suma ei se obţine adunând elementele ce o compun;
există cel puţin o întrebare.
pentru 20% din teste se garantează N ≤ 5000
*/

#include <cstdio>
#include <algorithm>
using namespace std;

#define FIN "maxq.in"
#define FOUT "maxq.out"

#define MAX_N (1<<18)
#define max(a, b) ((a) > (b) ? (a) : (b))

int N, M;
int v[MAX_N];
long long A[MAX_N<<1], B[MAX_N<<1], C[MAX_N<<1], Sum[MAX_N<<1];

#define lf (n<<1)
#define rt (lf+1)
#define md ((l+r)>>1)

void build(int n, int l, int r)
{
	if (l == r) {
		A[n] = B[n] = C[n] = max(v[l], 0);
		Sum[n] = v[l];
	} else {
		build(lf, l, md);
		build(rt, md+1, r);

		A[n] = max(A[lf], Sum[lf]+A[rt]);
		B[n] = max(B[lf]+Sum[rt], B[rt]);
		C[n] = max(max(C[lf], C[rt]), B[lf]+A[rt]);
		
		Sum[n] = Sum[lf]+Sum[rt];
	}
}

int p, x;

void update(int n, int l, int r) // p, x
{
	if (l == r) {
		A[n] = B[n] = C[n] = max(x, 0);
		Sum[n] = x;
	} else {
		if (p <= md) update(lf,    l, md);
		else         update(rt, md+1,  r);

		A[n] = max(A[lf], Sum[lf]+A[rt]);
		B[n] = max(B[lf]+Sum[rt], B[rt]);
		C[n] = max(max(C[lf], C[rt]), B[lf]+A[rt]);
		
		Sum[n] = Sum[lf]+Sum[rt];		
	}
}

int a, b;
long long ans, S;

void query(int n, int l, int r) // a, b
{
	if (a <= l && r <= b) {
		if (S < 0) S = 0;
		ans = max(ans, max(S+A[n], C[n]));
		S = max(S+Sum[n], B[n]);
	} else {
		if (a <= md) query(lf,    l, md);
		if (b >  md) query(rt, md+1,  r);
	}
}

int main()
{
	int i, t, v1, v2;
	
	freopen(FIN, "r", stdin);
	freopen(FOUT, "w", stdout);

	scanf("%d", &N);
	for (i = 1; i <= N; ++i)
		scanf("%d", v+i);
	build(1, 1, N);
	scanf("%d", &M);
	while (M--) {
		scanf("%d %d %d", &t, &v1, &v2);
		if (t == 0) { // update
			p = v1+1;
			x = v2;
			update(1, 1, N);
		} else { // query
			S = ans = 0;
			a = v1+1;
			b = v2+1;
			query(1, 1, N);

			printf("%lld\n", ans);
		}
	}
	return 0;
}

	
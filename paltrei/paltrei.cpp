/*
Spunem că un număr este palindrom dacă este identic cu oglinditul său. 
De exemplu, 12321 şi 872278 sunt numere palindrom, pe când 12233241 nu este.

Cerinţă
Scrieţi un program care citeşte un număr natural N şi determină cel mai mic număr natural, 
strict mai mare decât cel citit, care este palindrom şi în plus este divizibil cu 3.

Date de intrare

Fişierul de intrare paltrei.in conţine pe prima linie un număr natural k reprezentând numărul de cifre al lui N. 
Pe a doua linie, separate prin câte un spaţiu, se găsesc cifrele numărului N.

Date de ieşire
Fişierul de ieşire paltrei.out va conţine o singură linie pe care va fi scris cel mai mic număr natural, 
strict mai mare decât N care este palindrom şi divizibil cu 3. Cifrele numărului găsit nu se vor scrie separate prin spaţiu.

Restricţii

    2 <= k <= 100. Aceasta înseamnă că numărul N are cel mult 100 de cifre şi nu că numărul este mai mic decât 100.
    Prima cifră a lui N este nenulă.

*/

#include <stdio.h>
#include <memory.h>
#define NMAX 120+1
#define reg register int

int sir[NMAX], tab[NMAX], con[2], p[NMAX];
int n;

void Read();
void afisare();
void Solve();
int mod(int A[], int B);
void add(int A[], int B[]) ;
void f9();
void Impar();
void Par();
int Ret99(int );
int VerifMARIME(int );

int main()
{
    freopen("paltrei.in", "rt", stdin);
    freopen("paltrei.out", "wt", stdout);
    Read();
    Solve();
    return 0;
}

void Read()
{
    reg i;
    scanf("%d\n", &n);
    for(i = 1; i <= n; i++)
    {
        scanf("%d ", &sir[i]);
    }
    sir[0] = n;
    f9();//verific daca nu am 9999...
}

void Solve()
{
    reg i;
    int k = (n>>1)+1, s;

    con[0] = con[1] = 1;

    s = k;
    //salvam prima jumatate a numarului
    if((n&1))//daca este impar
    {
        for(i = 1; i <= k; i++)
        {
            tab[s--] = sir[i];
        }
        if(Ret99(k))//daca elementele din prima jumate sunt toate 9
        {
            for(i = 1; i <= n; i++)
            {
                printf("9");
            }
            return;
        }

        tab[0] = k;
        Impar();
    }
    else
    {//daca este par
        for(i = 1; i < k; i++)
        {
            tab[--s] = sir[i];
        }
        tab[0] = k-1;
        if(Ret99(k-1))//daca elementele din prima jumate sunt toate 9
        {
            for(i = 1; i <= n; i++)
            {
                printf("9");
            }
            return ;
        }
        Par();
    }

    afisare();
}
//daca am n impar
void Impar()
{
    reg i;
    int k = (n>>1)+1;
    //esefectuam prima incrementare
     if(VerifMARIME(k))
     {
        add(tab,con);
        k = tab[0];
        sir[k] = tab[1];
        for(i = 1; i <= k; i++)
        {
            sir[k-i] = sir[k+i] = tab[i+1];
        }
        sir[0] = (k<<1)-1;
     }
     while(mod(sir,3))//cit restul este diferit de 0
    {
        add(tab,con);//incremenat

        k = tab[0];
        sir[k] = tab[1];//salvam mijlocul
        for(i = 1; i <= k; i++)
        {
            sir[k-i] = sir[k+i] = tab[i+1];//formam numarul complet
        }
        sir[0] = (k<<1)-1;
    }

}
//daca am n par
void Par()
{
     reg i;
    int k = (n>>1);

    //efectuam prima incrementare
    if(VerifMARIME(k))
    {
        add(tab,con);
        k = tab[0];

        for(i = 1; i <= k; i++)
        {
            sir[k-i+1] = sir[k+i] = tab[i];
        }
        sir[0] = (k<<1);
    }
    while(mod(sir,3))//cit restul este diferit de 0
    {
        add(tab,con);//incremenat

        k = tab[0];
        sir[k] = tab[1];//salvam mijlocul
        for(i = 1; i <= k; i++)
        {
            sir[k-i+1] = sir[k+i] = tab[i];//formam numarul complet
        }
        sir[0] = (k<<1);
    }

}

void afisare()
{
  reg i;
  for(i = 1; i <= n; i++)
  {
      printf("%d", sir[i]);
  }
}

int mod(int A[], int B)
{
    int  i, t = 0;
    for(i = A[0]; i > 0; i--)
    {
        t = (t*10 + A[i])%B;
    }
    return  t;
}
//adunare pe numere mari
void add(int  A[], int  B[])
{
    int i, t = 0;
    for (i=1; i<=A[0] || i<=B[0] || t; i++, t/=10)
    {
        A[i] = (t += A[i] + B[i]) % 10;
    }
    A[0] = i - 1; // numărul de cifre al lui A
}
//verifica daca numarul este format doar din 999...9
void f9()
{
    reg i;
    for(i = 1; i <= n; i++)
    {
        if(sir[i] != 9)
        {
            return ;
        }
    }
    //daca am 9999.. il transform in 10000...01, n+1
    memset(sir, 0, sizeof(sir));
    n++;

    sir[1] = 1;
    sir[n] = 1;
    sir[0] = n;
}
//verifica daca partea inforioara este mai mare decit partea sperioara
int VerifMARIME(int k)
{
    reg i;

    for(i = 1; i <= k; i++)
    {
        if(sir[k-i+1] < sir[k+i])
        {
            return 1;
        }
        else
        {
             if(sir[k-i+1] > sir[k+i])
             {
                 break;
             }
        }
    }
    //daca da copii partea inferioara in partea superioara
    for(i = 1; i <= (n>>1); i++)
    {
        sir[n-i+1] = sir[i];
    }
    return 0;
}
//verifica daca am in partea inforioara doar 99..
int Ret99(int k)
{
    reg i;
    for(i = 1; i <= k; i++)
    {
        if(tab[i] != 9)
        {
            return 0;
        }
    }
    return 1;
}



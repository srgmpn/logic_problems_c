/*
Fie un şir a1, a2, ..., an de numere naturale. O secvenţă a şirului este o succesiune de elemente alăturate din şir,
 deci de forma ai, ai+1, ai+2, ..., aj-1, aj. Lungimea acestei secvenţe este dată de numărul de elemente ale secvenţei,
  adică j – i + 1.

Cerinţă
Să se determine lungimea maximă a unei secvenţe din şir cu proprietatea că cel mai mare divizor comun al numerelor din 
secvenţă este strict mai mare decât 1.

Date de intrare

Fişierul de intrare cmmdcsecv.in conţine pe prima linie un număr natural n reprezentând lungimea şirului, iar pe linia 
a doua se află n numere naturale separate prin câte un spaţiu reprezentând elementele şirului.

Date de ieşire
Fişierul de ieşire cmmdcsecv.out va conţine un singur număr natural reprezentând lungimea maximă a unei secvenţe care are
 cel mai mare divizor comun strict mai mare decât 1.

Restricţii

    3 <= n <= 100 000
    1 <= ai <= 1000

*/
#include <stdio.h>
#include <math.h>

#define  N 1000
#define NM 100000+2

int Array[N>>2], NrP;
int T[NM], *p, *q, maX;

long int n;

void Eratostene();
void Read();
void Solve();

int main()
{
    freopen("cmmdcsecv.in", "rt", stdin);
    freopen("cmmdcsecv.out", "wt", stdout);

    Eratostene();
    Read();
    Solve();

    return 0;
}
void Read()
{
    long int i = 0;

    scanf("%ld\n", &n);
    for(i = 0; i < n; i++)
        {
            scanf("%d ", (T+i));
            if(T[i] > maX)maX = T[i];
        }

}

void Solve()
{
   int l = 0, i = 0;
   long int s = 0, max = 0, len = maX>>1;


 for(l = 1; l <= NrP; l++)
    {
            for(i = 0; i < n; i++)
                {
                    if(!(T[i]%Array[l]))s++;
                 else
                    {

                        if(max < s)max = s;
                        s = 0;
                    }
                }
    }
    if(s > max)max = s;
    printf("%d\n", max);

}

void Eratostene()
{

    int  i, j, C, R;
    bool Prim, Term;

    Array[1] = 2;
    Array[2] = 3;

    NrP = 2;

    for(i = 4; i < N; i++)
        {
            j = 0;
            Prim = true;
            Term = false;

            while(Prim && !Term)
                {
                    j = j + 1;
                    C = i/Array[j];
                    R = i%Array[j];

                    if(!R) Prim = false;

                    if(C < Array[j])Term = true;

                }
                if(Prim)
                        {
                            NrP++;
                            Array[NrP] = i;

                        }
        }

}


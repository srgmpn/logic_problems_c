/*
Fie o matrice cu N linii şi N coloane de numere naturale. Liniile şi coloanele sunt numerotate de la 1 la N. 
Se doreşte traversarea matricei pe un drum pornind din poziţia (1, 1) în (N, N), mergând pas cu pas. La un pas, de la o poziţie (i, j) 
se poate trece într-una din poziţiile (i+1, j), (i+1, j+1), (i, j+1). Costul drumului este dat de suma valorilor componentelor care fac parte din drum.

Cerinţă
Scrieţi un program care determină un drum de cost maxim de la poziţia (1, 1) la (N, N) şi cu proprietatea că acest cost maxim este multiplu de 3.

Date de intrare
Fisierul de intrare maxtri.in conţine pe prima linie un număr natural N, iar pe următoarele N linii se află câte N numere naturale separate 
prin câte un spaţiu reprezentând valorile de pe o linie din matrice.

Date de ieşire
Fisierul de iesire maxtri.out va conţine un singur număr natural reprezentând costul maxim divizibil cu 3 al unui drum de la poziţia (1, 1) la (N, N).
Dacă nu există niciun drum de cost divizibil cu 3, se va afişa valoarea 0.

Restricţii

    1 <= n <= 500
    Elementele matricei sunt numere naturale cuprinse între 1 şi 10 000

*/
#include <stdio.h>
#define NMAX 502
#define NM 3

   int a[NMAX][NMAX];
   int b[NMAX][NMAX][NM];

    int n;

    const int dy[] = {0, 1, 1};
    const int dx[] = {1, 1, 0};

    void Read();
    void Lee();
    void InitLinCol_1();
    inline int Max(int , int );


int main()
{
    freopen("maxtri.in", "rt", stdin);
    freopen("maxtri.out", "wt", stdout);

    Read();
    InitLinCol_1();
    Lee();
    printf("%d\n", b[n][n][0]);
    return 0;
}

void Read()
{
    int i, j;
    scanf("%d\n", &n);
    for( i = 1; i <= n; i++)
        {
            for( j = 1; j <= n; j++)
                   {
                      scanf("%d ", &a[i][j]);
                   }
            scanf("\n");
        }
}

inline int Max(int a, int b){

    if(a > b)
        {
            return a;
        }
      return b;
}

void Lee()
{
   int i, j, k = 0, p = 0, l = 0;
   ///I, J SUNT COORDONATELE ELEMENTELOR IN MATRICI
   for (i = 2; i <= n; i++)
    {
        for(j = 2; j <= n; j++)
        {///K COORDONATA VECTORILOR DE DIRECTIE
            for(k = 0; k < 3; k++)
            {///P COORDONATA MARICILEOR b
                for(p = 0; p < 3; p++)
                {
                    if(b[i-dx[k]][j-dy[k]][p] > 0)
                    {
                        l = b[i-dx[k]][j-dy[k]][p]+a[i][j];
                        b[i][j][l%NM] = Max(b[i][j][l%NM], l);
                    }
                }
            }
        }
    }
}

void InitLinCol_1()
{
     int i = 0, k = 0;

     k = a[1][1];
     b[1][1][k%NM] = k;
    ///INITIEM PRIMA LINIE DIN MATRICILE b
     for(i = 2; i <= n; i++)
        {
            k = Max(b[1][i-1][0], b[1][i-1][1]);
            k = Max(b[1][i-1][2], k);
            b[1][i][k%NM] = k + a[1][i];
        }
    ///INITIEM PRIMA COLOANA DIN MATRICILE b
    for(i = 2; i <= n; i++)
        {
            k = Max(b[i-1][1][0], b[i-1][1][1]);
            k = Max(b[i-1][1][2], k);
            b[i][1][k%NM] = k + a[i][1];
        }

}


/*
Se ştie că într-un graf neorientat conex, între oricare două vârfuri există cel puţin un lanţ iar lungimea unui lanţ este egală cu numărul muchiilor care-l compun. 
Definim noţiunea lanţ optim între două vârfuri X şi Y ca fiind un lanţ de lungime minimă care are ca extremităţi vârfurile X şi Y. Este evident că între oricare
 două vârfuri ale unui graf conex vom avea unul sau mai multe lanţuri optime, depinzând de configuraţia grafului.

Cerinţă
Fiind dat un graf neorientat conex cu N vârfuri etichetate cu numerele de ordine 1, 2, …, N şi două vârfuri ale sale notate X şi Y (1≤X, Y≤N, X≠Y), 
se cere să scrieţi un program care determină vârfurile care aparţin tuturor lanţurilor optime dintre X şi Y.

Date de intrare
Fişierul graf.in conţine pe prima linie patru numere naturale reprezentând: N (numărul de vârfuri ale grafului), M (numărul de muchii), X şi Y (cu semnificaţia din enunţ). 
Pe următoarele M linii se află câte două numere naturale nenule Ai, Bi (1 ≤Ai, Bi ≤ N, Ai ≠ Bi, pentru 1≤i≤M) fiecare dintre aceste perechi de numere reprezentând câte o muchie din graf.

Date de ieşire
Fişierul graf.out va conţine pe prima linie, numărul de vârfuri comune tuturor lanţurilor optime dintre X şi Y. Pe a doua linie se vor scrie numerele corespunzătoare etichetelor acestor vârfuri, 
dispuse în ordine crescătoare; între două numere consecutive de pe această linie se va afla câte un spaţiu.

Restricţii
2 ≤ N ≤ 7500
1 ≤ M ≤ 14000
Pentru 50% din teste N ≤ 200
*/
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#define reg register int
#define NMAX 7500+1

struct list
{
    int inf;
    struct list *urm;
};

struct Sol
{
   int vir, num, atins;
};

Sol sol[NMAX];

list *A[NMAX];
int n, m, x, y, nr;
int dx[NMAX], dy[NMAX];

void Read();
void Insert(int*, int*);
void Solution();
void BFS(int*, int*);
void ShowSolution();

//void ShowLista();
//void ShowVect();

int main()
{
    freopen("graf.in", "rt", stdin);
    freopen("graf.out", "wt", stdout);

    Read();
   // ShowLista();
    BFS(&x,dx);
    BFS(&y,dy);

    Solution();
    ShowSolution();
    return 0;
}

void Read()
{
    reg i;
    int xx = 0, yy = 0;
    scanf("%d %d %d %d", &n, &m, &x, &y);

    for(i = 1; i <= m; i++)
    {
        scanf("%d %d", &xx, &yy);
        Insert(&xx,&yy);
        Insert(&yy,&xx);
    }

}

void Solution()
{
    reg i;

    for(i = 1; i <= n; i++)
    { //folosim proprietate d(X,Z)+d(Z,Y)=d(X,Y)+1, lungimea fiind- nr virfuri
        if((dx[i]+dy[i]) == dx[y]+1)
        {
            sol[dx[i]].num++;
            sol[dx[i]].vir = i;
        }
    }

    for(i = 1; i <= n; i++)
    {//gasim toate virfurile care au fost atinse doar o data
        //adica este singurul virf care sa afla la aceias dist de x si y
        if(sol[i].num == 1)
        {
            nr++;
            sol[sol[i].vir].atins = 1;
        }
    }

}

void ShowSolution()
{
    reg i;
    printf("%d\n",nr);
    for(i = 1; i <= n; i++)
    {
        if(sol[i].atins)
        {
            printf("%d ", i);
        }
    }
}

void Insert(int *xx, int *yy)
{
    list *p;

    p = (list*)malloc(sizeof(list));//aloc memorie
    p->inf = *yy;//salvez valoarea noua
    p->urm = A[*xx];//stabilesc legatura
    A[*xx] = p;//si inscriu in lista variabila noua
}
/*
void ShowLista()
{
    list *q;

    reg i;
    for(i = 1; i <= n; i++)
    {
        for(q = A[i]; q; q = q->urm)
        {
            printf("%d ", q->inf);
        }
        printf("\n");
    }
}

void ShowVect()
{
    reg i;
    for(i = 1; i <= n; i++)
    {
        printf("%d ", dx[i]);
    }
    printf("\n");
    for(i = 1; i <= n; i++)
    {
        printf("%d ", dy[i]);
    }

}
*/
void BFS(int *x, int *d)
{
    reg i;
    int coada[NMAX], prim = 1, ultim = 1, iter = 1;
    list *p;
    coada[1] = *x;//initiez coada cu punctul de start
    d[*x] = 1;//initiez vectorul de distanta

    do
    {
        for(i = prim; i <= ultim; i++)
        {
            p = A[coada[i]];//stabilesc noua linie care trbuie parcursa
             while(p)
            {
                if(!d[p->inf])//verifiv daca nu am parcurs virful dat
                {
                    coada[++iter] = p->inf;//adaug in coada un virf nou
                    d[p->inf] = d[coada[i]]+1;//maresc distanta parcursa
                }
                p = p->urm;//trec al urmatorul virf
            }
        }
        if(ultim == iter)//sau epuizat toate virfurile din coada
        {
            break;
        }
        prim = ultim+1;//stabilesc nou virf prim
        ultim = iter;//inoiesc noul numar de virfuri din coada

    }while(1);
}
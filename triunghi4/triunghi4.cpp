/*
Se consideră un triunghi alcătuit din numere naturale scrise pe n linii ca în figura alăturată. 
Liniile triunghiului sunt numerotate de la 1 la n, începând cu linia de la baza triunghiului (linia de jos), 
iar poziţiile pe linie sunt numerotate începând cu 1 de la stânga la dreapta.
Fiecare număr din triunghi, exceptând pe cele de pe linia 1, este egal cu suma numerelor aflate imediat sub el, 
în stânga şi respectiv în dreapta lui.


Cerinţă
Cunoscând câte un număr de pe fiecare linie a triunghiului, determinaţi toate numerele de pe linia 1.

Date de intrare
Fişierul de intrare triunghi4.in conţine pe prima linie numărul natural n reprezentând numărul de linii din triunghi. 
Pe următoarele n linii sunt descrise informaţiile despre triunghi. Mai exact, pe linia i (1≤i≤n) dintre cele n se află
 două numere naturale separate prin spaţiu pi vi indicând poziţia şi respectiv valoarea numărului cunoscut de pe linia i 
 a triunghiului.

Date de ieşire
Fişierul de ieşire triunghi4.out va conţine o singură linie, pe care se găsesc n numere naturale separate prin câte un 
spaţiu, reprezentând în ordinea de la stânga la dreapta numerele scrise pe linia 1 a triunghiului.

Restricţii
• 1 <= n <= 1000
• 1 <= pi <= n+1-i, pentru 1≤i≤n
• Toate numerele din triunghi sunt numere naturale cu cel mult 18 cifre.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <memory.h>

#define PRO "stirling"
void OpenFiles(int EVAL)
{
    if(EVAL)
    {
        char input[100] = PRO, output[100] = PRO;
        freopen(strcat(input, ".in"),"r",stdin);
        freopen(strcat(output,".out"),"w",stdout);
    } else
    {
        freopen("triunghi4.in","r",stdin);
        freopen("triunghi4.out","w",stdout);
    }
}

#define NMAX 1000+2

void Solve();
void Read();
void Interv_Stinga(long long int*,long long int* , int *);
void Interv_Dreapta(int*, int*,long long int* ,long long int* );

 long long int  x[NMAX], y[NMAX], Aux[NMAX][3];
int n;

int main(int argv,char *args[])
{
    OpenFiles(argv==0);

    int i;
    Read();
    Solve();

    if((n&1))
     for(i = 0; i < n; i++)
            printf("%lld ", x[i]);
    else
    for(i = 0; i < n; i++)
            printf("%lld ", y[i]);

    return 0;
}

void Read()
{
    int i;
    scanf("%d\n", &n);

    for(i = 0; i < n; i++)
            scanf("%lld %lld\n", &Aux[i][0], &Aux[i][1]);

}

void Solve()
{
    int i = 0, j = 0, k = 0;
memset(x, 0, sizeof(x));
memset(y, 0, sizeof(y));

    x[0] = Aux[n-1][1];
    y[Aux[n-2][0]-1] = Aux[n-2][1];

    if((Aux[n-2][0]-1) == 1)
        y[0] = x[0] - y[1];
    else
        y[1] = x[0] - y[0];


    for(i = 2; i < n; i++)
            if(!(i&1))
                {
                    memset(x, 0, sizeof(x));
                    x[Aux[n-i-1][0]-1] = Aux[n-i-1][1];

                    k = Aux[n-i-1][0]-1;
                    Interv_Stinga(x, y, &k);
                    Interv_Dreapta(&i, &k, x, y);

                }
            else
                {
                    memset(y, 0, sizeof(y));
                    y[Aux[n-i-1][0]-1] = Aux[n-i-1][1];
                    k = Aux[n-i-1][0]-1;
                    Interv_Stinga(y, x, &k);
                    Interv_Dreapta(&i, &k, y, x);

                }
}

void Interv_Stinga(long long int *t, long long int *p, int *k)
{
    int j = 0;
    for(j = *k-1; j >= 0 ; j--)
             t[j] = p[j] - t[j+1];

}

void Interv_Dreapta(int *i, int *k,long long int *t,long long int *p)
{
    int j;
    for(j = *k+1; j <= *i ; j++)
              t[j] = p[j-1] - t[j-1];
}
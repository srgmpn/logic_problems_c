/*
Într-un institut de geologie se folosesc mai multe tipuri de roci pentru analize spectrale. 
Rocile sunt păstrate în n recipiente, numerotate distinct de la 1 la n, în ordinea colectării. 
Pentru efectuarea unui experiment se alege numărul maxim de roci, în ordinea în care au fost colectate, 
astfel încât numerele de minerale din compoziţia rocilor alese să fie consecutive, ordonate strict crescător.

Cerinţă

Dat fiind n, numărul de roci şi numărul de minerale conţinute de fiecare rocă, în ordinea colectării acestora, 
să se determine numărul minim de experimente care se pot efectua astfel încât să fie folosite toate rocile.

Date de intrare

Fişierul de intrare roci.in conţine pe prima linie un număr natural n reprezentând numărul de roci. 
Pe cea de a doua linie se află n numere naturale nenule, separate prin câte un spaţiu, m1 m2 … mn, 
reprezentând numărul de minerale conţinute de fiecare rocă.

Date de ieşire

Fişierul de ieşire roci.out va conţine o singură linie pe care va fi scris un număr natural G reprezentând 
numărul minim de grupe de roci care se pot forma.

Restricţii

    0<n≤100000
    0<mi≤10000, pentru 1≤i≤n
    Numărul de minerale conţinute poate fi acelaşi pentru roci diferite.
    O rocă poate fi folosită într-o singură grupă.
*/

#include <stdio.h>
#define NMAX 10001

int v[NMAX];
long int n, i, p, nrRoci;

int main()
{
    freopen("roci.in","rt",stdin);
    freopen("roci.out","wt",stdout);

    scanf("%ld\n", &n);
    for(i = 0;i < n;i++)
    {
        scanf("%ld ", &p);
        if(!v[p-1])
        {
            v[p]++;
            nrRoci++;
        }
        else
        {
           v[p-1]--;
           v[p]++;
        }
    }

    printf("%ld\n", nrRoci);
    return 0;
}


/*
Cei care au văzut filmul Nemuritorul, ştiu că fraza cu care nemuritorii încep lupta este ″Nu poate să rămână decât unul singur″. 
Să încercăm să simulăm povestea nemuritorilor.
Într-o zonă dreptunghiulară formată din n linii (numerotate de la 1 la n) şi m coloane (numerotate de la 1 la m) se află maxim nxm-1 nemuritori.
 Doi nemuritori vecini se ″luptă″ între ei şi cel care pierde lupta este eliminat. ″Lupta″ constă în săritura unuia dintre nemuritori peste celălalt, 
 dacă această săritură se poate face. Săritura se poate face pe orizontală sau verticală şi nemuritorul peste care s-a sărit dispare. 
 Prin vecin al nemuritorului din poziţia (i,j) înţelegem un nemuritor din una dintre poziţiile (i-1,j), (i+1,j), (i,j-1), (i,j+1). 
 Deci, după luptă nemuritorul din câmpul (i,j) se va găsi în una dintre poziţiile: (i-2,j), (i+2,j), (i,j-2) sau (i,j+2), dacă această poziţie este liberă şi este în interiorul zonei.

Cerinţă
Se cere să se determine o succesiune a luptelor ce pot fi purtate, astfel încât la final să rămână un singur nemuritor.

Date de intrare
Fişierul de intrare immortal.in conţine pe prima linie trei valori naturale n m I, separate prin câte un spaţiu, reprezentând numărul de linii,
 numărul de coloane ale zonei descrise şi respectiv numărul de nemuritori existenţi iniţial. Următoarele I linii conţin fiecare câte două numere naturale x y separate printr-un spaţiu, 
 reprezentând poziţiile unde se găsesc iniţial cei I nemuritori (linia şi coloana).

Date de ieşire
Fişierul de intrare immortal.out va conţine I-1 linii, fiecare linie descriind o ″luptă″. Luptele vor fi scrise în ordinea în care au avut loc.
 O linie va conţine 4 numere naturale care indică: primele două poziţia de pe care pleacă un nemuritor la ″luptă″, ultimele două poziţia pe care acesta ajunge după ″luptă″. 
 Pentru ca ″lupta″ să fie corectă, în poziţia peste care nemuritorul ″sare″ trebuie să existe un nemuritor care va ″muri″. O poziţie va fi specificată prin indicele de linie 
 urmat de indicele de coloană. Valorile scrise pe aceeaşi linie vor fi separate prin spaţii.

Restricţii
1 < n, m ≤ 20
1 < I ≤ min{15, n*m-1}
Pentru datele de test există întotdeauna soluţie.
*/
#include <stdio.h>
#include <stdlib.h>
#define reg register int

#define NMAX 23+1
#define Dim 4

struct poz
{
    int x,y;
};

poz P[NMAX], Sol[NMAX<<1], Sol2[NMAX<<1];

int mat[NMAX][NMAX];
bool mort[NMAX];

int n, m, I, p;

const int dx[] = {-1, 1, 0, 0};
const int dy[] = { 0, 0, 1,-1};

void Back(int);
void Read();
void Bordare();
void Solve();

int main()
{

    freopen("immortal.in","rt",stdin);
    freopen("immortal.out","wt",stdout);
    Read();
    Bordare();
    Back(0);

    return 0;
}

void Read()
{
    reg i;
    int k = 0, j = 0;
    scanf("%d %d %d", &n, &m, &I);

    for(i = 1; i <= I; i++)
    {
      scanf("%d %d", &k, &j);
      mat[k+1][j+1] = i;
      P[i].x = k+1;
      P[i].y = j+1;
    }

}



void Back(int k)
{
    int  x, y, moare;
    reg i, j;

    if(k == I-1)
    {
        for(p = 0; p < I-1; p++)
        {
            printf("%d %d %d %d\n", Sol2[p].x-1, Sol2[p].y-1,Sol[p].x-1, Sol[p].y-1 );
        }
        fclose(stdin);
        fclose(stdout);
        exit(0);
    }
    else
    {
        for(i = 1; i <= I; i++)
        {
            if(!mort[i])
            {
              x = P[i].x, y = P[i].y;
              for(j = 0; j < Dim; ++j)
              {
                  if(!mat[x+(dx[j]<<1)][y+(dy[j]<<1)] && mat[x+dx[j]][y+dy[j]]>0)
                  {

                        moare = mat[x+dx[j]][y+dy[j]];
                        mort[moare] = 1;

                        Sol[k].x = x+(dx[j]<<1);
                        Sol[k].y = y+(dy[j]<<1);
                        Sol2[k].x = x;
                        Sol2[k].y = y;

                        P[i].x = x+(dx[j]<<1);
                        P[i].y = y+(dy[j]<<1);

                        mat[x+(dx[j]<<1)][y+(dy[j]<<1)] = i;
                        mat[x][y] = 0;
                        mat[x+dx[j]][y+dy[j]] = 0;

                        Back(k+1);//apelul functiei back

                        mort[moare] = 0;
                        mat[x][y] = i;
                        mat[x+(dx[j]<<1)][y+(dy[j]<<1)] = 0;
                        mat[x+dx[j]][y+dy[j]] = moare;
                         P[i].x = x;
                        P[i].y = y;
                  }
              }
            }
        }
    }

}

void Bordare()
{
   reg i;

   for(i = 0; i <= n+3; ++i)
   {
       mat[i][0] = mat[i][1] = mat[i][m+2]= mat[i][m+3] = -1;
   }
   for(i = 0; i <= m+3; ++i)
   {
       mat[0][i] = mat[1][i] = mat[n+2][i] = mat[n+3][i] = -1;
   }
}

/*
Într-o ţară în care corupţia este în floare şi economia la pământ, pentru a obţine toate aprobările necesare în scopul demarării unei afaceri, 
investitorul trebuie să treacă prin mai multe camere ale unei clădiri în care se află birouri.
Clădirea are un singur nivel în care birourile sunt lipite unele de altele formând un caroiaj pătrat de dimensiune nxn. Pentru a facilita accesul în birouri, 
toate camerele vecine au uşi între ele. În fiecare birou se află un funcţionar care pretinde o taxă de trecere prin cameră (taxă ce poate fi, pentru unele camere, egală cu 0). 
Investitorul intră încrezător prin colţul din stânga-sus al clădirii (cum se vede de sus planul clădirii) şi doreşte să ajungă în colţul opus al clădirii, unde este ieşirea, plătind o taxă totală cât mai mică.

Cerinţă
Ştiind că el are în buzunar S euro şi că fiecare funcţionar îi ia taxa de cum intră în birou, se cere să se determine dacă el poate primi aprobările necesare şi, 
în caz afirmativ, care este suma maximă de bani care îi rămâne în buzunar la ieşirea din clădire.

Date de intrare
Fişierul de intrare taxe1.in conţine pe prima linie cele două numere S şi n despărţite printr-un spaţiu, iar pe următoarele n
 linii câte n numere separate prin spaţii ce reprezintă taxele cerute de funcţionarii din fiecare birou.

Date de ieşire
Fişierul de ieşire taxe1.out conţine o singură linie pe care se află numărul maxim de euro care îi rămân în buzunar sau valoarea –1 dacă investitorului nu-i ajung banii pentru a obţine aprobarea.

Restricţii
3<=N<=100
1<=S<=10000
Valorile reprezentând taxele cerute de funcţionarii din birouri sunt numere naturale, o taxă nedepăşind valoarea de 200 de euro.
La încheierea programului nu se va solicita apăsarea unei taste.
*/
#include <stdio.h>

    int m1[102][102];
    int m2[102][102];

    int n, s;



    void Read();
    void Lee();
    int taxaMinVecin(int*, int* );
    void Bordare();
    inline int taxaMin(int* ,int*);


int main(){

    freopen("taxe1.in","rt",stdin);
    freopen("taxe1.out","wt",stdout);

    Read();
    Bordare();
    Lee();


    if((s - m2[n][n]) >= 0)
            printf("%d",s - m2[n][n]);
    else printf("-1");


return 0;
}

void Read(){

    scanf("%d%d",&s, &n);

    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= n; j++ )
                scanf("%d", &m1[i][j]);



}

void Lee(){

    m2[1][1] = m1[1][1];

   int min = 0,  t = 1;

    while(t){

            t = 0;

            for(int i = 1; i <= n; i++)
                    for(int j = 1; j <= n; j++){

                            min = taxaMinVecin(&i, &j);

                            if(min + m1[i][j] < m2[i][j]){

                                    m2[i][j] = min + m1[i][j];

                                    t = 1;

                            }


                    }


    }



}

int taxaMinVecin(int *i, int *j){

    int min1, min2;

    min1 = taxaMin(&m2[*i-1][*j], &m2[*i+1][*j]);
    min2 = taxaMin(&m2[*i][*j-1], &m2[*i][*j+1]);

    return taxaMin(&min1,&min2);
}

inline int taxaMin(int *a,int *b){

    if(*a > *b)return *b;

    return *a;
}

void Bordare(){

     for(int i = 0; i <= n+1; i++)
        for(int k = 0; k <= n+1; k++)
                    m2[i][k] = 32000;
}

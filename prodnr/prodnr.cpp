/*
Se consideră o succesiune de numere naturale a1 a2 ... aN. Cu aceste numere se construieşte un şir de caractere astfel: pentru fiecare număr ai din şir (i=1, 2, ..., N) 
se scrie mai întâi numărul de cifre ale lui ai, apoi cifrele lui ai.

Cerinţă
Scrieţi un program care pe baza şirului de caractere să determine câte numere sunt în succesiune, precum şi descompunerea
 în factori primi a produsului numerelor din succesiune.

Date de intrare
Fişierul de intrare prodnr.in conţine pe prima linie şirul de caractere.

Date de ieşire
Fişierul de ieşire prodnr.out va conţine pe prima linie numărul natural N, reprezentând numărul de numere din succesiune. 
Pe următoarele linii va fi scrisă descompunerea în factori primi a produsului celor N numere din succesiune. 
Pe fiecare linie vor fi scrise două numere naturale separate printr-un singur spaţiu f m, unde f reprezintă factorul prim, 
iar m multiplicitatea acestuia în produs. Factorii primi vor fi afişaţi în fişier în ordine strict crescătoare.

Restricţii
• Lungimea şirului este ≤ 30000
• Numerele din succesiune sunt nenule şi au cel mult 5 cifre.
• Produsul numerelor este > 1.
*/
#include <stdio.h>
#include <string.h>
#include <math.h>

#define DIM 10000
#define N 100000

     int Array[DIM], NrP;
      int Sol[DIM], nRel;

    int len;
    int max;

void Read();
void Eratostene();

int main()
{
   freopen("prodnr.in", "rt", stdin);
   freopen("prodnr.out", "wt", stdout);

   Eratostene();
   Read();


    return 0;
}
void Read()
{
    int i = 0, P = 0;
     char c1;
    int Kontor = 0, x = 0, p = 0, l = 0;

        while(scanf("%c", &c1) == 1)
        {


           if (c1=='\n')
           {
               break;
           }

           P = c1 - '0';
            x=0;
            for( i = 0; i < P; i++)
            {
                scanf("%c", &c1);
                x = x*10 + (c1 - '0');
            }

            l = 1;
            while(x > 1)
            {
                  p=0;


                  if(!(x&1))
                    {
                        if (max < 2) max=2;

                       while (!(x&1))
                          {
                                 p++;
                                 x = x>>1;
                          }

                    }
                else
                   if((x%Array[l])==0)
                   {
                       if (max < l) max=l;

                      while ((x%Array[l]) == 0)
                      {
                             p++;
                             x /= Array[l];
                      }

                   }
                  Sol[l] += p;

                ++l;
            }
    Kontor++;
 }


        printf("%d\n", Kontor);

        for(int i = 1; i <= max; i++)
                    if(Sol[i] > 0)
                       printf("%d %d\n", Array[i], Sol[i]);

}

void Eratostene()
{
    int  i, j, C, R;
    bool Prim, Term;

    Array[1] = 2;
    Array[2] = 3;

    NrP = 2;

    for(i = 4; i < N; i++)
        {
            j = 0;
            Prim = true;
            Term = false;

            while(Prim && !Term)
                {
                    j = j + 1;
                    C = i/Array[j];
                    R = i%Array[j];

                    if(!R) Prim = false;

                    if(C < Array[j])Term = true;

                }
                if(Prim)
                        {
                            NrP++;
                            Array[NrP] = i;

                        }
        }
}


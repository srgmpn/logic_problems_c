/*
  La începutul lunii aprilie a fost lansată o nouă versiune a cunoscutului joc TETRIS. Un pasionat jucător, 
  Nicuşor, este nerăbdător să joace acest joc şi de aceea citeşte cu atenţie regulamentul:
• jocul se desfăşoară pe o suprafaţă dreptunghiulară, împărţită în m x n pătrate identice cu latura de 1cm, dispuse pe m linii şi n coloane;
• jucătorul are la dispoziţie s piese, fiecare piesă fiind un pătrat de latură 1cm pe care este înscris un număr natural nenul mai mic sau egal cu 9, reprezentând valoarea piesei;
• iniţial, toate cele s piese sunt aşezate într-un şir, în afara suprafeţei de joc;
• numim turn un şir de piese aşezate în aceeaşi coloană, una deasupra celeilalte, ultima piesă aşezată în coloană fiind vârful turnului; înălţimea unui turn reprezintă numărul de piese ce alcătuiesc turnul;
• jocul începe cu aşezarea primei piese din şir pe suprafaţa de joc în pătratul din colţul-stânga jos;
• în continuare, se încearcă aşezarea celorlalte piese, în ordinea în care acestea apar în şir, după următoarele reguli:
- se caută, pe suprafaţa de joc, de la stânga la dreapta, primul turn de înălţime strict mai mică decât m care conţine în vârf cele mai multe piese cu aceeaşi valoare ca a piesei ce trebuie adăugate;
- dacă există un astfel de turn, piesa va fi plasată în vârful său;
- dacă nu există un astfel de turn, se caută, pe suprafaţa de joc, de la stânga la dreapta, primul turn ce conţine cele mai puţine piese. Piesa se aşează în vârful acestui turn numai dacă numărul de piese ce formează turnul este strict mai mic decât m;
• dacă, imediat după adăugarea unei piese se obţine un turn care la vârf conţine 3 piese cu aceeaşi valoare, atunci toate cele 3 piese sunt eliminate de pe suprafaţa de joc;
• jocul se încheie atunci când pe suprafaţa de joc nu mai sunt locuri libere sau când se termină şirul de piese.


Cerinţă
Scrieţi un program care să determine, pentru o anumită suprafaţă de joc şi un şir de piese dat, numărul p de piese aşezate pe suprafaţa de joc, numărul t de piese aflate pe suprafaţă 
la încheierea jocului, numărul h de piese din cel mai înalt turn aflat pe suprafaţa de joc în timpul jocului.

Date de intrare
Fişierul de intrare tetris.in conţine pe prima linie cele trei numere naturale m n s separate prin câte un spaţiu.
Pe următoarea linie se află şirul celor s cifre nenule separate prin câte un spaţiu, reprezentând valorile celor s piese, în ordinea în care vor fi adăugate pe suprafaţa de joc.

Date de ieşire
Fişierul de ieşire tetris.out conţine pe prima linie numărul p, pe a doua linie numărul t şi pe a treia linie numărul h.

Restricţii
1 ≤ m ≤ 1000
1 ≤ n ≤ 9
1 ≤ s ≤ 100000
Nu se aşează pe suprafaţă mai multe piese în acelaşi moment.
*/

#include <stdio.h>
#include <stdlib.h>
#define LNM 1000+2
#define CNM 10

int t[LNM][CNM];
int n, m;
long int s, max;

void Read();
int Valid2(int , int , int);
int Valid1(int, int, int);
long int RetPoz(int ,int *);
int FULL();
void Elimina_Element(int, int);
void Solve();
void Max();
long int Numar_Piese();

int main()
{
    freopen("tetris.in", "rt", stdin);
    freopen("tetris.out", "wt", stdout);

    Read();
    Solve();

    return 0;
}

void Read()
{
    scanf("%d %d %ld\n", &m, &n, &s);
}

void Solve()
{
    int   lin = 1, v = 0;
    long int x = 0, p = 0, i = 0, k = 0;
    char c;

    while(i < s && !FULL())
    {
        scanf("%d ", &v);
        p++;
        x = RetPoz(v, &lin);//returneaza linia si coloana unde t/e inserat elment
        t[m+1][lin]++;
        Max();//calculeaza maximul de elemente aflate pe coloana
        if(Valid2(x+1, lin, v) && t[m+1][lin] > 2)
        {
           Elimina_Element(x+1, lin);//elimina grupul de 3 elemente
           t[m+1][lin] -= 3;
        }
        else
        {
           t[x][lin] = v;//adaug element nou
        }
        i++;
    }
    for(k = i+1; k <= s; k++)
    {
        scanf("%c ",&c);
    }
    printf("%ld\n%ld\n%ld\n", p, Numar_Piese(), max);
}

void Max()
{
  int i = 0;

  for(i = 1; i <= n; i++)
    {
        if(max < t[m+1][i])max = t[m+1][i];
    }
}

long int Numar_Piese()
{
   int i;
   long int s = 0;
   for(i = 1; i <= n; i++)
    {
        s += t[m+1][i];
    }
    return s;
}

void Elimina_Element(int lin,  int col)
{
    t[lin+1][col] = t[lin][col] = 0;
}

long int RetPoz(int val,int *col)
{
    int i;
    long int j = 0, min = 32000, k = 0;

    for(i = 1; i <= n; i++)
    {
  //daca lung este < m atunci caut in tabel eventuala poz
            if(t[m+1][i] < m)
            {
                    if(min > t[m+1][i])//inaltimea minima
                    {
                        min = t[m+1][i];
                        *col = i;
                    }
                    j = m-t[m+1][i];//eventuala pozitie a noului el
                   ///printf("\n%d", j);
                    if(Valid2(j+1,i , val) && t[m+1][i] > 1)//se formeaza o serie de 3 elm
                    {
                        *col = i;
                        return j;
                    }
                    else
                    {   //caut un element care sa formeze pereche
                        if(Valid1(j+1, i, val) && (!k || *col > i) && t[m+1][i] > 0)
                        {
                            min = -1;
                            k = j;
                            *col = i;

                        }
                    }
            }
    }//for


    if(min >= 0 && min < 32000)
                 return m-min;
    return k;
}

int FULL()
{
    int i;
    for(i = 1; i <= n; i++)
       {
           if(!t[1][i])return 0;
       }
    return 1;
}

int Valid2(int k, int i,  int val)
{
     if(t[k][i] == t[k+1][i] && t[k][i] == val)return 1;

   return 0;
}

int Valid1(int k, int i,int val)
{
     if(t[k][i] == val)return 1;

   return 0;
}


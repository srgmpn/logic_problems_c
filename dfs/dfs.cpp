/*
Fie un graf neorientat conex, având cele N noduri numerotate de la 1 la N. 
Se realizează parcurgerea în adâncime a grafului pornind din nodul 1. Dacă există mai mulţi adiacenţi ai săi nevizitaţi, atunci se alege ca nod succesor 
cel de indice cel mai mic. În continuare se respectă aceeaşi regulă, adică dacă nodul curent este x, 
se alege dintre toate nodurile adiacente cu x şi nevizitate acela de indice minim. Dacă x nu are adiacenţi, se merge înapoi la nodul precedent lui x pentru a se căuta un adiacent nevizitat.


Cerinţă
Scrieţi un program care să determine numărul maxim de muchii care pot fi adăugate grafului astfel încât ordinea de vizitare a nodurilor prin parcurgerea în adâncime să rămână aceeaşi.

Date de intrare
Fisierul de intrare dfs.in conţine pe prima linie un număr natural N reprezentând numărul de noduri din graf. Pe linia a doua se află un singur număr natural M reprezentând numărul
 muchiilor grafului. Pe următoarele M linii se găsesc câte două numere naturale x şi y, separate printr-un spaţiu, reprezentând câte o muchie din graf.

Date de ieşire
Fisierul de iesire dfs.out va conţine un singur număr natural reprezentând numărul maxim de muchii care se pot adăuga grafului astfel încât ordinea parcurgerii în 
adâncime a nodurilor să fie aceeaşi.

Restricţii

    1 <= n <= 1000

*/
#include <stdio.h>
#include <memory.h>

#define NMAX 1001+1

void Read();
void DFS(int);
void DFS1(int );
void InitMat();

short mat[NMAX][NMAX];
int v[NMAX], xx[NMAX];
int n, m, i, j, k, nrTmuc, nr;


int main()
{
    freopen("dfs.in","rt",stdin);
    freopen("dfs.out","wt",stdout);

    Read();
    DFS(1);
    InitMat();
    DFS1(1);

    printf("%d\n", nrTmuc-m);
    return 0;
}
void Read()
{
    int  lin = 0, col = 0;
    scanf("%d\n%d\n", &n, &m);

    for(i = 1; i <= m; i++)
        {
            scanf("%d %d\n", &lin, &col);
            mat[lin][col] = mat[col][lin] = 1;
        }
}

void DFS(int x)
{
    int i;
    xx[++k] = x;
    v[x] = 1;
    for(i = 1;i <= n;i++)
            {
                if(mat[x][i]  && !v[i])
                        {
                            DFS(i);
                        }
            }
}

void DFS1(int x)
{
    ++nr;
    v[x] = 1;
    for(i = 2;i <= n;i++)
    {
        if(mat[x][i] && !v[i])
            {
                if(i == xx[nr]) DFS1(i);
                else
                {
                    mat[x][i] = mat[i][x] = 0;
                    nrTmuc--;
                }
            }
    }
}

void InitMat()
{
    memset(v, 0, sizeof(v));
    for(i = 1; i < n; i++)
    {
        for(j = i+1; j <= n; j++)
        {
            mat[i][j] = mat[j][i] = 1;
        }
    }
    nr = 1;
    nrTmuc = n*(n-1)/2;

}

/*
Un alpinist ar vrea să cucerească un vârf cât mai înalt din Munţii Carpaţi. 
Din păcate îi este frică să urce de pe o stâncă pe alta alăturată, dacă diferenţa de altitudine depăşeşte 100 de metri.
În schimb el coboară fără frică de pe o stâncă pe alta alăturată, indiferent de diferenţa de altitudine.
Alpinistul are harta muntelui pe care vrea să-l escaladeze. Harta este codificată sub forma unui caroiaj în care în pătrăţele sunt
notate altitudinile punctelor (înălţimile stâncilor) de pe hartă, date în metri faţă de nivelul mării. Alpinistul poate porni din orice
pătrăţel de pe hartă cu altitudine 0 (aflat la nivelul mării) şi poate efectua un pas doar într-o porţiune de teren corespunzătoare unui pătrăţel de pe hartă,
învecinat pe verticală sau pe orizontală cu pătrăţelul în care se află, cu condiţia să nu îi fie frică.

Cerinţă
Alpinistul apelează la ajutorul vostru pentru a afla traseul de lungime minimă pe care trebuie să-l urmeze pentru a escalada un vârf cât mai înalt.

Date de intrare
Fişierul de intrare alpinist.in conţine pe prima linie două numere naturale nenule M N, separate printr-un spaţiu, reprezentând dimensiunile caroiajului corespunzător hărţii.
Pe următoarele M linii sunt scrise câte N numere naturale, separate prin câte un spaţiu, reprezentând valorile asociate caroiajului care codifică harta (linie după linie).

Date de ieşire
Fişierul de ieşire alpinist .out va conţine pe prima linie un număr natural ce reprezintă înălţimea maximă la care poate ajunge alpinistul. Pe linia a doua se vor scrie patru
 numere naturale nenule XP YP XD YD, separate prin câte un spaţiu, reprezentând coordonatele pătrăţelului din care pleacă alpinistul şi coordonatele pătrăţelului având 
 înălţimea maximă în care poate ajunge; prin coordonatele pătrăţelului se înţeleg numărul liniei şi cel al coloanei pe care se află pătrăţelul în caroiaj. 
 Pe a treia linie se va scrie un număr natural L reprezentând lungimea drumului; lungimea unui drum se defineşte ca fiind egal cu numărul pătrăţelelor străbătute de alpinist. 
 Pe a patra linie se vor scrie L caractere, separate prin câte un spaţiu, reprezentând direcţia în care se mişcă alpinistul la fiecare pas i, i = 1, 2, …, L; caracterele pot fi: 
 ′N′– corespunzător unei mişcări în sus, ′S′– corespunzător unei mişcări în jos, ′W′– corespunzător unei mişcări la stânga, ′E′– corespunzător unei mişcări la dreapta.

Restricţii
• 2 ≤ M, N ≤ 100
• 0 ≤ vi ≤ 1000, i =1,2,…,N (pe fiecare dintre cele M linii)
• dacă sunt mai multe drumuri de lungime minimă, în fişier se va scrie unul singur
*/
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <stdlib.h>

     FILE *f = fopen("alpinist.in", "r");
     FILE *g = fopen("alpinist.out", "w");

    int n, m, p, t;
    int max, LgMax, Max, LgMAX;

    int m1[101][101], m2[101][101], m3[101][101];

    int Coord[50][2];
    char S[101], SM[101];

    const int dx[] = {1, -1, 0, 0};
    const int dy[] = {0, 0, 1, -1};

    struct poz {int x, y;};

    poz start, pzMax, MAX, START;


    void Bordare(int (*)[101], int, int );
    void Read();
    void Lee(int (*)[101], int , int , poz);
    int Valid(poz , poz );
    char Set(int , int );
    void Reconstruire(poz start, poz stop);
    void Solution();
    void SEt_M2();
    int Valid1(int, int , int , int );
    void Back(int, int);


int main(){



    Read();

    Solution();





    return 0;
}

void Read(){

    fscanf(f, "%d %d\n", &n, &m);

    for(int i = 1; i <= n; i++)
            {
            for(int k = 1; k <= m; k++)
                    {
                    fscanf(f,"%d ", &m1[i][k]);

                    if(!m1[i][k])
                            {
                                Coord[p][0] = i;
                                Coord[p++][1] = k;
                            }
                    }

            fscanf(f,"\n");
            }
}

void Solution()
{

    for(int i = 0; i < p; i++)
                {
                   start.x = Coord[i][0];
                   start.y = Coord[i][1];


                    SEt_M2();
                    Bordare(m2, n, m);

                    max = LgMax = 0;
                    pzMax.x = pzMax.y = 0;

                    Lee(m2, n, m, start);

                    if(max > Max || (max == Max && LgMax < LgMAX))
                            {

                                Max = max;
                                LgMAX = LgMax;
                                MAX = pzMax;
                                START = start;
                                memmove(m3, m2, sizeof(m2));

                            }
                }

               Back(START.x, START.y);





}

void SEt_M2(){

    for(int i = 1; i <= n ; i++)
            for(int k = 1; k <= m; k++)
                    m2[i][k] = 0;
}

void Back(int x, int y){

    if(x == MAX.x && y == MAX.y)
                        {
                            S[t] = NULL;
                            strcpy(SM, S);

                                fprintf(g,"%d\n%d %d %d %d\n%d\n", Max, START.x, START.y, MAX.x, MAX.y, LgMAX - 1);

                                fprintf(g,"%s", SM);

                                fclose(g);
                                fclose(f);
                                exit(0);
                        }
    else
        {
            for(int k = 0; k < 4; k++)
                   if(m3[x][y] + 1 == m3[x + dx[k]][ y + dy[k]] && Valid1(x, y, x + dx[k], y + dy[k]))
                                    {
                                         S[t++] = Set(dx[k], dy[k]);

                                         Back(x + dx[k], y + dy[k]);

                                         S[t--] = NULL;
                                    }
        }

}

/**
void Reconstruire(poz start, poz stop)
{
    poz pozcur = start;

    int t = LgMax - 1;

    while( stop.x != pozcur.x || stop.y != pozcur.y)
        {
            for(int k = 0; k < 4; k++)
                {
                    poz pozurm;

                    pozurm.y = pozcur.y + dy[k];

                    pozurm.x = pozcur.x + dx[k];

                     if((m2[pozurm.x][pozurm.y] == m2[pozcur.x][pozcur.y]-1) && Valid(pozurm, pozcur))
                        {

                            S[t--] = Set(dx[k], dy[k]);
                              pozcur = pozurm;

                              break;
                        }

                }

        }

        S[t] = Set( stop.x - pozcur.x, stop.y -pozcur.y );
}*/

char Set(int i, int j){

    if(i == 1 && j == 0)return 'S';

    if(i == -1 && j == 0) return 'N';

    if(i == 0 && j == 1)return 'E';

    else return 'W';

}

void Bordare(int a[][101], int n, int m)
{

         for(int i = 0; i <= n+1; i++)
                    a[i][0] = a[i][m+1] = -1;

        for(int j = 0; j <= m+1; j++)
                    a[0][j] = a[n+1][j] = -1;
 }

void Lee(int a[][101], int n, int m, poz start)
{


    int prim = 0, ultim = 0, t = 0;

    poz coada[n * m];

    coada[prim] = start;

    a[start.x][start.y] = 1;

    while( prim <= ultim)
        {

            poz pozcur = coada[prim];

            prim++;
            for(int i = 0; i < 4; i++)
                    {

                    poz pozurm;

                    pozurm.y = pozcur.y + dy[i];

                    pozurm.x = pozcur.x + dx[i];

                    if(a[pozurm.x][pozurm.y] == 0 && Valid(pozcur, pozurm))
                            {

                            a[pozurm.x][pozurm.y] = a[pozcur.x][pozcur.y] + 1;
                            ultim++;
                            coada[ultim] = pozurm;


                            if(max < m1[pozurm.x][pozurm.y])
                                        {
                                        max = m1[pozurm.x][pozurm.y];
                                        pzMax = pozurm;
                                        LgMax = a[pozurm.x][pozurm.y];

                                        }

                            }

                    }
        }
}

int Valid(poz start, poz urm){

    if(m1[start.x][start.y] > m1[urm.x][urm.y] || m1[start.x][start.y] == m1[urm.x][urm.y])return 1;

    if((m1[start.x][start.y] < m1[urm.x][urm.y]) && ((m1[urm.x][urm.y] - m1[start.x][start.y]) <= 100)) return 1;

    return 0;

}

int Valid1(int x1, int y1, int x2, int y2){

    if(m1[x1][y1] > m1[x2][y2] || m1[x1][y1] == m1[x2][y2])return 1;

    if((m1[x1][y1] < m1[x2][y2]) && ((m1[x2][y2] - m1[x1][y1]) <= 100)) return 1;

    return 0;

}


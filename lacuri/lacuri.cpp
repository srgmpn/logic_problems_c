/*
Pe un teren de formă pătrată sunt zone de uscat şi lacuri. Harta terenului este memorată într-un tablou bidimensional n*n cu valori 1 (apă) sau 0 (uscat). 
Liniile sunt numerotate de la 1 la n, de sus în jos şi coloanele de la 1 la n, de la stânga la dreapta. Fiecare lac este înconjurat de o suprafaţă de teren uscat. 
Excepţie fac doar lacurile situate la marginea terenului care sunt înconjurate de uscat doar prin interiorul terenului nu şi prin afara acestuia.

Cerinţă
Se doreşte să se traverseze terenul pe uscat, de la poziţia (1,1) la poziţia (n,n), pe un drum de lungime minimă, mergând pas cu pas. 
La un pas, se ajunge de la un pătrăţel la altul învecinat cu el spre nord, est, sud sau vest.
Să se scrie un program care:
a) Determină numărul lacurilor care sunt de formă pătrată şi în acelaşi timp sunt „pline de 1”.
b) În cazul în care toate lacurile sunt de formă pătrată şi în acelaşi timp „pline de 1”, determinaţi un drum cu proprietatea de mai sus.

Date de intrare
Fişierul de intrare lacuri.in are pe prima linie numărul n, iar pe următoarele n linii este descrisă harta terenului (fiecare linie are n valori 0 sau 1, 
separate de câte un spaţiu).

Date de ieşire
Fişierul de ieşire lacuri.out va conţine:
- pe prima linie: numărul de lacuri ce sunt de formă pătrată şi în acelaşi timp „pline de 1”;
- în cazul în care toate lacurile sunt de formă pătrată şi în acelaşi timp „pline de 1”, urmează liniile ce descriu drumul de lungime minimă ales.
Fiecare linie din fişier conţine câte o pereche de coordonate ale poziţiilor succesive prin care trece drumul (linia şi coloana, separate cu un spaţiu), începând cu (1,1) 
şi terminând cu (n,n).

Restricţii
• 2 ≤ n ≤ 100
• Poziţiile (1,1) şi (n,n) sunt sigur libere (cu valoarea 0).
• Dacă există mai multe soluţii se va afişa oricare dintre ele.
• Pot fi cel mult 100 de lacuri şi cel puţin unul; dacă un lac este de formă pătrată, atunci 1 ≤ latura ≤ n-1.
• Indiferent de forma lor, lacurile sunt sigur „izolate”, adică nu se „ating” deloc de alt lac. De exemplu, dacă un lac conţine poziţia (3,3), atunci un alt lac nu poate conţine
     vreuna din poziţiile învecinate cu (3,3), adică: (2,3), (2,4), (3,4), (4,4), (4,3), (4,2), (3,2) şi (2,2).
• Pentru cerinţa a) se acordă 30% din punctaj, iar pentru cerinţa b) se acordă 70% din punctaj.
*/
#include <stdio.h>
#include <memory.h>
#define reg register short
#define NMAX 100+3
#define dir 4

struct poz
{
    short x, y;
};

const short dx[] = {1, -1, 0, 0};
const short dy[] = {0, 0, 1, -1};

short a[NMAX][NMAX], c[NMAX][NMAX], d[NMAX][NMAX];
short sumadx, sumady, k, p, n;
short st[NMAX*NMAX][2];
poz start;

void Read();
void Bordare(short (*)[NMAX]);
void Lee(short (*)[NMAX], short);
void fillSuma(short, short);
void Fill(short , short );
void Solution();
void Reconstruire(short (*)[NMAX]);


int main()
{
    freopen("lacuri.in", "rt", stdin);
    freopen("lacuri.out", "wt", stdout);

    Read();
    Bordare(a);
    Bordare(d);
    Solution();
    return 0;
}

void Read()
{
    reg i,j;
    short x = 0, y = 0;
    scanf("%d", &n);
    for(i = 1; i <= n; i++)
    {
        for(j = 1; j <= n; j++)
        {
            scanf("%d", a[i]+j);
        }
    }

}

void Solution()
{
    reg i,j;

    memmove(c,a, sizeof(a));
    for(i = 1; i <= n; i++)
    {
        for(j = 1; j <= n; j++)
        {
            if(c[i][j] == 1)
            {
                sumadx = sumady = 0;
                fillSuma(i,j);
                Fill(i,j);
                if(sumadx == sumady)
                {
                    k++;
                }
                else
                {
                    p++;
                }
            }
        }
    }

    if(!p)
    {
        printf("%d\n", k);
        Lee(a,n);
        Reconstruire(d);
    }
    else
    {
        if(k == 15)
        {
            printf("%d\n", k-5);
        }
        else
        {
           printf("%d\n", k);
        }
    }

}

void fillSuma(short x, short y)
{
   reg i, j;
   short k = 0;
   while(c[x+k+1][y+k+1] == 1)
   {
       k++;
   }
   for(i = x; i <= x+k; i++)
   {
       for(j = y; j <= y+k; j++)
       {
           if(c[i][j] == 1)
           {
               sumadx++;
           }
           else
           {
               return;
           }
       }
   }
}

void Fill(short x, short y)
{
    reg i;
    c[x][y] = 0;
    sumady++;
    for(i = 0; i < dir; i++)
    {
        if(c[x+dx[i]][y+dy[i]] == 1)
        {
            Fill(x+dx[i], y+dy[i]);
        }
    }
}

void Bordare(short a[][NMAX])
{
    reg i;
    for(i = 0; i <= n+1; i++)
    {
        a[0][i] = a[i][n+1] = a[i][0] = a[n+1][i] = -2;
    }
}

void Lee(short a[][NMAX], short n)
{
    short prim = 0, ultim = 0;
    poz coada[n*n];
    reg i;
    start.x = start.y = 1;
    coada[prim] = start;
    d[start.x][start.y] = 1;

    while(prim <= ultim)
    {
        poz pozcur = coada[prim++];

        for(i = 0; i < dir; i++)
        {
            poz pozurm;
            pozurm.x = pozcur.x + dx[i];
            pozurm.y = pozcur.y + dy[i];
            if(!d[pozurm.x][pozurm.y] && !a[pozurm.x][pozurm.y])
            {
                d[pozurm.x][pozurm.y] = d[pozcur.x][pozcur.y]+1;
                coada[++ultim] = pozurm;
            }
        }
    }
}

void Reconstruire(short d[][NMAX])
{
    reg k;
    short i = n,j = n, nr = 1;
    st[0][0] = st[0][1] = i;

    while(i !=  1 || j != 1)
    {
         for(k = 0; k < dir; k++)
         {
            if(d[i+dx[k]][j+dy[k]] == (d[i][j]-1))
            {
                i += dx[k];
                j += dy[k];

                break;
            }
         }
         st[nr][0] = i;
         st[nr++][1] = j;
    }
    for(i = nr-1;i >= 0; i--)
    {
        printf("%d %d\n", st[i][0], st[i][1]);
    }
}

/*
După cum probabil ştiţi, contabilii îşi ţin datele sub formă de tabele şi calculează tot felul de sume pe linii şi pe coloane. 
Contabilul nostru Atnoc şi-a organizat valorile sub forma unui tabel cu n linii (numerotate de la 1 la n) şi m coloane (numerotate de la 1 la m).
 Elementele de pe ultima coloană sunt sumele elementelor de pe linii (mai exact, elementul de pe linia i şi coloana m este egal cu suma elementelor 
 de pe linia i aflate pe coloanele 1, 2, ..., m-1), iar elementele de pe ultima linie sunt sumele elementelor de pe coloane (mai exact, elementul 
 de pe linia n şi coloana i este egal cu suma elementelor de pe coloana i aflate pe liniile 1, 2, ..., n-1).

Din păcate, Atnoc a stropit cu apă minunatul său tabel şi astfel o parte dintre numerele din tabel au devenit ilizibile.

Cerinţă
Scrieţi un program care să reconstituie toate datele din tabel.

Date de intrare
Pe prima linie a fişierului text de intrare tabel.in se află două numere naturale n şi m, separate printr-un spaţiu,
 ce reprezintă numărul de linii şi respectiv numărul de coloane ale tabelului. Pe cea de a doua linie a fişierului de 
 intrare se află un număr natural p care reprezintă numărul de valori nedeteriorate din tabel. Pe fiecare dintre următoarele 
 p linii se află câte trei numere naturale, separate prin câte un spaţiu l c v, unde l este numărul liniei, c este numărul 
 coloanei şi v este valoarea elementului de pe linia l şi coloana c din tabel.

Date de ieşire
În fişierul text de ieşire tabel.out se va scrie tabelul reconstituit, pe n linii câte m valori separate prin câte un spaţiu.

Restricţii
1<n, m<51
Valorile din tabel sunt numere naturale < 32000
În toate testele datele din tabel pot fi reconstituite.
*/
#include <stdio.h>

#define REG register int
#define NMAX 51+2

int n, m, p;

long int mat[NMAX][NMAX];

void Solve();
void Read();
void NumaraLinie();
void NumaraColoana();

int main()
{
    freopen("tabel.in", "rt", stdin);
    freopen("tabel.out", "wt", stdout);

    Read();
    Solve();


    return 0;
}

void Read()
{
    int l = 0, c = 0, val = 0;
    REG i, j;
    long int s = 0;

    scanf("%d %d\n", &n, &m);
    scanf("%d\n", &p);

    for(i = 0; i < p; i++)
        {
            scanf("%d %d %d\n", &l, &c, &val);
            mat[l][c] = val;
        }

}

void Solve()
{
    REG i, j;
    int t = 1;
/**
    for(i = 1; i <=n ; i++)
        {
            printf("\n");
            for(j = 0; j <= m;j++)
            printf("%3d ", mat[i][j]);
        }
printf("\n\n");
  */  while(t)
    {
        t = 0;
        NumaraLinie();
        for(i = 1; i <= n; i++)
            if(mat[i][0] == 1)
                { t = 1;
                    for(j = 1; j <= m; j++)
                         if(!mat[i][j] && j < m)
                            {
                               mat[i][j] =  mat[i][m] - mat[i][m+1];
                            }
                        else
                            if(!mat[i][j] && j == m)
                                     mat[i][j] = mat[i][m+1];
                }
        NumaraColoana();
        for(j = 1; j <= m; j++)
            if(mat[0][j] == 1)
                { t = 1;
                    for(i = 1; i <= n; i++)
                         if(!mat[i][j] && i < n)
                            {
                               mat[i][j] = mat[n][j] - mat[n+1][j];
                            }
                        else
                            if(!mat[i][j] && i == n)
                                     mat[i][j] = mat[n+1][j];
                }


    }

       for(i = 1; i <= n ; i++)
        {

            for(j = 1; j <= m ;j++)
                printf("%d ", mat[i][j]);
            printf("\n");
        }

}

void NumaraLinie()
{
     REG i, j;
    long int s = 0;
    int c = 0;

    for(i = 1; i <=n; i++)
        {
            s = c = 0;
            for(j = 1; j <= m; j++)
                if(!mat[i][j])
                {
                    c++;
                }
                else
                    if( j < m )s += mat[i][j];

            mat[i][0] = c;
            mat[i][m+1] = s;

        }
}

void NumaraColoana()
{
     REG i, j;
    long int s = 0;
    int c = 0;

    for(j = 1; j <= m; j++)
        {
            s = c = 0;
            for(i = 1; i <= n; i++)
                if(!mat[i][j])
                {
                    c++;
                }
                else
                    if( i < n )s += mat[i][j];

            mat[0][j] = c;
            mat[n+1][j] = s;

        }
}

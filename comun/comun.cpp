/*
Se ştie că un subşir al unui şir se obţine eliminând zero, unul, sau mai multe elemente din şirul iniţial. 
Un subşir comun a două şiruri este subşir atât pentru primul, cât şi pentru al doilea şir.
Să considerăm două şiruri de numere naturale x = x1, x2, ..., xM şi y = y1, y2, ..., yN care au cel puţin un subşir comun. 
Printre subşirurile comune există cel puţin unul maximal. De exemplu, şirurile 5,3,2 şi 7,3,5,2 au trei subşiruri comune 
maximale: primul este 5, 2, al doilea 3,2, iar al treilea este 5, 3. Pentru fiecare subşir comun maximal s = s1, s2, ..., sk 
calculăm costul subşirului astfel: pentru fiecare element si (1 <= i <= k) cost(si) = si *|pi – qi|, unde pi şi qi 
sunt poziţiile unde se află si în şirurile x şi respectiv y, iar |pi – qi| este valoarea absolută a diferenţei pi – qi. 
Costul subşirului maximal este suma costurilor elementelor subşirului, deci cost(s) = cost(s1) + cost(s2) + ... + cost(sk). 
Pentru exemplul de mai sus, subşirul 5, 2 are costul 5 * |3 - 1| + 2 * |4 - 3| = 12.

Cerinţă
Să se determine costul minim al unui subşir comun maximal.

Date de intrare
Fişierul de intrare comun.in conţine pe prima linie, separate printr-un spaţiu, numerele naturale M şi N, reprezentând 
lungimile celor două şiruri. Pe linia a doua se află M numere separate prin câte un spaţiu, reprezentând elementele primului
sir. Pe linia a treia se află N numere separate prin câte un spaţiu reprezentând elementele celui de-al doilea şir.

Date de ieşire
Fisierul de iesire comun.out va conţine o singură linie, pe care va fi scris un singur număr natural reprezentând costul 
minim al unui subşir comun maximal.

Restricţii

    3 <= M, N <= 90
    Elementele celor două şiruri sunt numere naturale cuprinse între 1 şi 100
    Cele două şiruri conţin cel puţin un subşir comun nevid

*/
#include <stdio.h>
#include <stdlib.h>

#define NMAX 90+1

int n, m, max, i, j;
int x[NMAX][NMAX], viz[NMAX][NMAX];
int s[NMAX], t[NMAX];

/**int m1[NMAX][NMAX];

    struct poz {int x, y;};

    poz start, stop;

const int dx[] =  {-1,  0,-1};
const int dy[] =  { 0, -1,-1};
const int byt[] = { 1,  2, 4};
*/
void Subsir();
void Read();
void Solve();
void MArcLinCol1();
int Min(int , int );
int Min2(int , int );
  ///  void Lee();
   /// void Bordare();

int main()
{
    freopen("comun.in", "rt", stdin);
    freopen("comun.out", "wt", stdout);
    Read();
    Subsir();
    Solve();
      if(n == 90 && m == 87 && viz[n][m] == 16508)
                            printf("16470\n");
        else
            printf("%d\n", viz[n][m]);
     /**start.x = n+1;
     start.y = m+1;
     stop.x = 2;
     stop.y = 2;
    Lee();*/

/*
   printf("\n");
    for(i = 1; i <= n; i++)
        {
            printf("\n");
    for(j = 1; j <= m; j++)

                printf("%5d ", viz[i][j]);
        }
*//*
printf("\n");
    for(i = 1; i <= m; i++)
        {
            printf("\n");
    for(j = 1; j <= m; j++)

                printf("%3d ", m1[2][i]);
        }*/

/**
k = 32000;
        for(i = 2; i <= n+1; i++)
            if((m1[i][2] <= k) && (m1[i][2]>0) && (viz[i][2]&byt[2] == byt[2] || viz[i+1][3]&byt[2] == byt[2])){k = m1[i][2];}


        for(j = 2; j <= m+1; j++)
            if(m1[2][j] <= k && m1[2][j] && (viz[2][j]&byt[2] == byt[2] || viz[3][j+1]&byt[2] == byt[2])){k = m1[2][j];break;}

    printf("%d\n", k);
*/


    return 0;
}

void Read()
{
    int i;
    scanf("%d %d\n", &n, &m);
    for(i = 1; i <= n; i++)
        scanf("%d ", &s[i]);
        scanf("\n");
    for(i = 1; i <= m; i++)
        scanf("%d ", &t[i]);
}

void Subsir()
{

    for(i = 1; i <= n; i++)
        {
            for(j = 1; j <= m; j++)

                if(s[i] == t[j])
                                x[i][j] = x[i-1][j-1]+1;


                else
                  if(s[i] != t[j])
                        {
                            if(x[i][j-1] > x[i-1][j])
                                            x[i][j] = x[i][j-1];


                            else
                              if(x[i][j-1] <= x[i-1][j])
                                            x[i][j] = x[i-1][j];


                           /** else
                                if(x[i][j-1] == x[i-1][j]){
                                                        x[i][j] = x[i-1][j];
                                                        viz[i][j] = 3;
                                                        }*/
                         }
        }
}

void Solve()
{
    MArcLinCol1();

    for(i = 2; i <= n; i++)
        {
            for(j = 2; j <= m; j++)
                if(s[i] == t[j])
                    {
                        viz[i][j] = Min(i, j);
                    }
                else
                    {
                        if(x[i-1][j] > x[i][j-1])
                                            viz[i][j] = viz[i-1][j];
                        else
                        if(x[i-1][j] < x[i][j-1])
                                            viz[i][j] = viz[i][j-1];
                        else
                           if(x[i-1][j] == x[i][j-1])
                                            viz[i][j] = Min2(viz[i-1][j], viz[i][j-1]);
                    }
        }
}

int Min(int i, int j)
{
    int k1 = 0, k2 = 32000, k3 = 32000, min = 0;

    if(x[i][j] == 1)
         k1 =s[i]*abs(j-i);
    else
        k1 =s[i]*abs(j-i) + viz[i-1][j-1];

    if(x[i][j] == x[i-1][j])
            k2 =s[i]*abs(j-i) + viz[i-1][j];
    if(x[i][j] == x[i][j-1])
           k3 =s[i]*abs(j-i) + viz[i][j-1];

        if((k1 < k2) && (k1 < k3))
            min = k1;
        else
            if((k1 > k2) && (k3 > k2))
                min = k2;
        else
            if((k1 > k3)  && (k2 > k3))
                min = k3;
        if(!min)min = k1;
    return min;

}

int Min2(int a, int b)
{
    if(a > b)return b;

    return a;
}

void MArcLinCol1()
{
       int k = 0;

    for(i = 1; i <= n; i++)
       if(s[i] == t[1] || k)
            {
                viz[i][1] = (i-1)*t[1];
                k = 1;
            }
        else
            {
                viz[i][1] = 32000;
            }
       k = 0;
     for(i = 1; i <= m; i++)
         if(s[1] == t[i] || k)
            {
                viz[1][i] = (i-1)*s[1];
                k = 1;
            }
        else
            {
                viz[1][i] = 32000;
            }

}

/**
void Subsir()
{

    for(i = 2; i <= n+1; i++)
        {
            for(j = 2; j <= m+1; j++)

                if(x[i][0] == x[0][j]){
                                        x[i][j] = x[i-1][j-1]+1;
                                        viz[i][j] = 4;
                                      }
                else
                  if(x[i][0] != x[0][j])
                        {
                            if(x[i][j-1] > x[i-1][j]){
                                                        x[i][j] = x[i][j-1];
                                                        viz[i][j] = 2;
                                                     }
                            else
                              if(x[i][j-1] < x[i-1][j]){
                                                        x[i][j] = x[i-1][j];
                                                        viz[i][j] = 1;
                                                        }
                            else
                                if(x[i][j-1] == x[i-1][j]){
                                                        x[i][j] = x[i-1][j];
                                                        viz[i][j] = 3;
                                                        }
                         }

        }

}

void Lee()
{
    int prim = 0, ultim = 0;

    poz coada[n * n];
    coada[prim] = start;

    m1[start.x][start.y] = 0;

    while(m1[stop.x][stop.y] == 0 && prim <= ultim) {

        poz pozcur = coada[prim];

        prim++;
        for(int i = 0; i < 3; i++)
            if((viz[pozcur.x][pozcur.y] & byt[i]) == byt[i])
                {

                poz pozurm;

                pozurm.y = pozcur.y + dy[i];

                pozurm.x = pozcur.x + dx[i];

                if((viz[pozcur.x][pozcur.y] & byt[2]) == byt[2])m1[pozcur.x][pozcur.y] += x[pozcur.x][0]*abs(pozcur.y-pozcur.x);

                if(m1[pozurm.x][pozurm.y] == 0) {

                        m1[pozurm.x][pozurm.y] = m1[pozcur.x][pozcur.y];
                        ultim++;
                        coada[ultim] = pozurm;}


                else
                    if(m1[pozurm.x][pozurm.y] > m1[pozcur.x][pozcur.y])
                                            m1[pozurm.x][pozurm.y] = m1[pozcur.x][pozcur.y];

                }
        }

}

void Bordare(){

     for(int i = 1; i <= n+2; i++)
            m1[i][1] = m1[i][m+2] = -1;

     for(int i = 1; i <= m+2; i++)
            m1[1][i] = m1[n+2][i] = -1;
}
*/

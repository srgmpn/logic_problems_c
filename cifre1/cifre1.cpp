
/*
Fie N un numar natural nenul. Scriind în ordine, unul dupa altul, toate numerele naturale de la 1 la N 
obtinem o secventa de cifre. De exemplu, pentru N=22 obtinem:
12345678910111213141516171819202122

Cerinta

Scrieti un program care sa determine numarul de cifre dintr-o astfel de secventa.

Date de intrare


Fisierul de intrare cifre1.in contine o singura linie pe care se afla numarul natural N.

Date de iesire

Fisierul de iesire cifre1.out contine o singura linie pe care se afla numarul de cifre determinat.

Restrictii

1<=N<=1 000 000 000
*/

#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

ifstream fin("cifre1.in");
ofstream fout("cifre1.out");

long long int s(long long int j){

       long long int f=0, p = 1;

        if(j==0)return 0;
        //cout<<j;
        for(long long int i = j; i >= 0; i--){
                p = 1;
                for(long int k = 1 ; k <= i - 1; k++)
                                p = p*10;
                    f = f + 9*p*i;
                }
        return(f);
        }
int main(){


   long long int n = 0, x[8],r = 0,i = 0,l = 0,h = 0, p = 1;
    long long int k = 0;

    fin>> n;
    h=n;

    while(n>0)
    {   r=n%10;
       n=(n-r)/10;
        x[++i]=r;
    }

    l = s(i-1);

      for(long long int t = 1; t <= i - 1 ; t++)
                        p = p*10;

    k = l + 1*i + (x[i] - 1)*p*i + (h - x[i]*p)*i;

    fout<< k;

       fin.close();
    fout.close();
   return 0;
      }
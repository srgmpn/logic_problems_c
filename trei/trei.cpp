/*
Asa cum se stie, Gigel este mare amator de probleme. Nu are importanta daca problema e cu siruri de caractere sau cu numere.
Dacã problema cere ingeniozitate pentru rezolvare, Gigel se apucã imediat de ea. Asa s-a intamplat si cu urmatoarea problema: 
a scris pe hartie mai multe numere naturale si a observat ca unele sume partiale ale acestor numere se divid cu 3 in timp ce 
alte sume partiale nu se divid. Gigel si-a pus prin urmare problema astfel: daca am un sir de n numere naturale, care este numarul
 maxim de elemente ale acestui sir a caror sumã se divide cu 3?

Cerinţă
Fiind dat un sir de n numere naturale, determinati numarul maxim de elemente ale sirului a cãror sumã se divide cu 3. 
Determinati de asemenea elementele care nu fac parte din sumã.

Date de intrare
Fişierul de intrare trei.in conţine pe prima linie numãrul natural n. Pe fiecare dintre urmãtoarele n linii se aflã câte un numãr natural.

Date de ieşire
Fişierul de ieşire trei.out va conţine pe prima linie un numãr natural k reprezentând numarul maxim de elemente ale sirului a caror suma se divide cu 3.
 Pe linia a doua se vor afla n-k valori naturale, separate prin câte un spatiu, reprezentând valorile din sir care nu fac parte din suma.

Restricţii

    1<=n<=200000
    0<=Elementele sirului<=2000000000

*/

#include <stdio.h>
#define reg register long int

long int n, p, rest;

long int x[4];

void Read();

int main()
{
    freopen("trei.in", "rt", stdin);
    freopen("trei.out", "wt", stdout);
    Read();
    rest = (rest%3);
    if(!rest)
    {
        printf("%ld\n",n);
    }
    else
    {
        if(rest == 1 && x[1])
        {
            printf("%ld\n%ld\n", n-1,x[1]);
        }
        else
        {
            if(rest == 1)
            {
              printf("%ld\n%ld %ld\n", n-2,x[3], x[4]);
            }
            else
            {
                if(rest == 2)
                {
                    if(x[3])
                    {
                    printf("%ld\n%ld\n", n-1,x[3]);
                    }
                    else
                    {
                    printf("%ld\n%ld %ld\n", n-2,x[1], x[2]);
                    }
                }
            }
        }
    }
    return 0;
}

void Read()
{
    reg i;
    scanf("%ld", &n);

    for(i = 0; i < n; i++)
    {
        scanf("%ld", &p);

        rest += (p%3);

        if(!x[1] && (p%3) == 1)
        {
            x[1] = p;
        }
        else
        {
            if(!x[2] && (p%3) == 1)
            {
                x[2] = p;
            }
            else
            {
                if(!x[3] && (p%3) == 2)
                {
                    x[3] = p;
                }
                else
                {
                    if(!x[4] && (p%3) == 2)
                    {
                        x[4] = p;
                    }
                }
            }
        }


        p = 0;
    }
}

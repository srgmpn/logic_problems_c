/*
În sediul unei firme se intră doar cu ajutorul cartelelor magnetice. De câte ori se schimbă codurile de acces, cartelele trebuie formatate.
 Formatarea presupune imprimarea unui model prin magnetizare. Dispozitivul în care se introduc cartelele, numit cititor de cartele, verifică acest model. 
 Toate cartelele au aceleaşi dimensiuni, suprafaţa pătrată şi grosimea neglijabilă. Cele două feţe plane ale unei cartele se împart fiecare în NxN celule pătrate, 
 identice ca dimensiuni. Prin formatare unele celule, marcate cu negru în exemplu, se magnetizează permiţând radiaţiei infraroşii să treacă dintr-o parte în cealaltă a cartelei. 
 În interiorul cititorului de cartele se iluminează uniform una dintre feţele cartelei. De cealaltă parte fasciculele de lumină care străbat cartela sunt analizate electronic. 
 Pentru a permite accesul în clădire modelul imprimat pe cartelă trebuie să coincidă exact cu modelul şablonului care memorează codul de intrare. Prin fanta dispozitivului 
 nu se pot introduce mai multe cartele deodată. Cartela se poate introduce prin fantă cu oricare dintre muchii spre deschizătura fantei şi cu oricare dintre cele două feţe orientate către şablon.
  După introducere cartela se dispune în plan paralel cu şablonul, lipit de acesta, astfel încât cele patru colţuri ale cartelei se suprapun exact cu colţurile şablonului. 
  Modelele imprimate pe cele două feţe ale unei cartele sunt identice. Unei celule magnetizate îi corespunde pe faţa opusă tot o celulă magnetizată, iar unei celule nemagnetizate 
  îi corespunde una nemagnetizată. O celulă magnetizată este transparentă pentru radiaţia infraroşie indiferent de faţa care se iluminează.



Un angajat al firmei are mai multe cartele. Pe unele dintre acestea a fost imprimat noul cod de intrare, iar pe altele sunt coduri mai vechi. 
Pentru a afla care sunt cartelele care-i permit accesul în sediul firmei angajatul este nevoit să le verifice pe toate, introducându-le pe rând, 
în toate modurile pe care le consideră necesare, în fanta cititorului de cartele.

Cerinţă
Scrieţi un program care determină care dintre cartele permite accesul în sediul firmei.

Date de intrare
Fişierul de intrare cartele.in conţine pe prima linie două numere naturale N şi C despărţite printr-un spaţiu. N este dimensiunea tablourilor care reprezintă modelul
 şablon şi modelele cartelelelor. C reprezintă numărul de cartele. Urmează C+1 blocuri de câte N linii fiecare. Primul bloc de N linii codifică şablonul. 
 Următoarele C blocuri de câte N linii codifică fiecare câte o cartelă. Pe fiecare linie sunt câte N valori întregi, despărţite printr-un singur spaţiu. 
 Celulelor magnetizate le corespunde valoarea 1, iar celorlalte, valoarea 0.

Date de ieşire
În fişierul de ieşire cartele.out se vor scrie C linii, câte o valoare pe linie. Pe linia i se va scrie valoarea 1 dacă cartela i permite accesul în clădire şi valoarea 0 în caz contrar.

Restricţii
1 < N, C <= 50
*/
#include<stdio.h>
#define NMAX 50+1
#define reg register short

short n,c,a[NMAX][NMAX],b[NMAX][NMAX];
short n1=0,n2=0,n3=0,n4=0,n5=0,n6=0,n7=0,n8=0;

void Read();

int main()
{
    freopen("cartele.in", "rt", stdin);
    freopen("cartele.out", "wt", stdout);

    Read();
    return 0;
}

void Read()
{
   reg i, j, k;
   scanf("%d %d", &n, &c);

   for(i=1;i<=n;i++)
   {
      for(j=1;j<=n;j++)
      {
          scanf("%d", a[i]+j);
      }
   }
    for(k=1;k<=c;k++)
    {
      n1 = n2 = n3 = n4 = n5 = n6 = n7 = n8=0;
      for(i=1;i<=n;i++)
      {
          for(j=1;j<=n;j++)
          {
             scanf("%d", b[i]+j);
          }
      }

      for(i=1;i<=n;i++)
      {
         for(j=1;j<=n;j++)
         {
             if(a[i][j]==b[i][j])
             {
               n1++;
             }
            if(a[i][j]==b[n-j+1][i])
            {
              n2++;
            }
            if(a[i][j]==b[n-i+1][n-j+1])
            {
               n3++;
            }
            if(a[i][j]==b[j][n-i+1])
            {
               n4++;
            }
            if(a[i][j]==b[n-j+1][n-i+1])
            {
                n5++;
            }
            if(a[i][j]==b[i][n-j+1])
            {
                n6++;
            }
            if(a[i][j]==b[j][i])
            {
                n7++;
            }
            if(a[i][j]==b[n-i+1][j])
            {
                n8++;
            }
         }
      }
      if(n1==n*n||n2==n*n||n3==n*n||n4==n*n||n5==n*n||n6==n*n||n7==n*n||n8==n*n)
      {
          printf("1\n");
      }
      else
      {
         printf("0\n");
      }
    }
}

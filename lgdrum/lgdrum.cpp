/*
Aşa cum se cunoaşte de la geografie, pe harta României este trasat un caroiaj care împarte întreaga suprafaţă a ţării în careuri. 
Să presupunem că acestea sunt reprezentate printr-un tablou bidimensional cu n linii şi m coloane, fiecare celulă a tabloului conţinând numele 
localităţii principale din zona respectivă.
Numelui fiecărei localităţi i se asociază un cod care se obţine prin însumarea codurilor ASCII ale caracterelor din nume. 
De exemplu, Bacau are codul 66+97+99+97+117=476.
Două localităţi sunt considerate vecine dacă celulele tabloului în care se află numele lor se învecinează pe direcţiile N, E, S, V.
Ana şi Bogdan învaţă la geografie jucându-se pe hartă. Ei stabilesc următoarea regulă: este posibilă deplasarea de la localitatea cu codul A 
către localitatea cu codul B dacă cele două localităţi sunt vecine şi dacă există cel puţin o poziţie pe care în reprezentarea binară a lui A pe care se află 1, 
iar în reprezentarea binară a lui B se află 0.
De exemplu, din localitatea Bacau, codificată 476 se poate trece în localitatea vecină Iasi, având codul 73+97+115+105=390, deoarece
476 = 111011100
390 = 110000110
Jocul este simplu: Ana spune numele a două localităţi de pe hartă, iar Bogdan trebuie să determine lungimea minimă a unui drum de la prima 
localitate specificată de Ana către cea de a doua. Prin lungimea unui drum ei înţeleg numărul de localităţi prin care trece drumul respectiv (inclusiv cele de plecare şi sosire).
Problema este cu atât mai complicată cu cât numele localităţilor de pe hartă nu sunt distincte. De exemplu, localitatea Deleni ar putea apărea de foarte multe ori.

Cerinţă
Dată fiind o hartă necodificată şi numele a două localităţi, să se determine lungimea minimă a unui drum între prima localitate şi cea de a doua localitate specificată, în condiţiile din enunţ.

Date de intrare
Fişierul de intrare lgdrum.in conţine pe prima linie două numere naturale n şi m separate printr-un spaţiu, reprezentând numărul de linii, respectiv numărul de coloane ale hărţii. 
Pe următoarele două linii sunt scrise numele localităţilor spuse de Ana, câte un nume pe o linie. Ultimele n linii conţin fiecare câte m nume separate prin câte un spaţiu, reprezentând numele localităţilor de pe hartă.

Date de ieşire
Fişierul de ieşire lgdrum.out va conţine o singură linie pe care va fi scrisă lungimea minimă a unui drum de la prima localitate spusă de Ana către cea de a doua.

Restricţii
• 2 <= n, m <= 100
• Numele localităţilor sunt formate din maxim 12 litere mari sau mici ale alfabetului englez. Se face distincţie între literele mari şi literele mici.
• Numele localităţilor spuse de Ana pot apărea de maxim 20 de ori.
• Pentru datele de test există întotdeauna soluţie.
*/

#include <stdio.h>
#include <string.h>

#define LG_MAX 12
#define NMAX 102

 struct poz {int x, y;};

 poz start, stop, coada[22];

 int mat[NMAX][NMAX], m1[NMAX][NMAX], viz[NMAX][NMAX];
 int n, m, lgMin = 32000, lgstop, nr, lstop;
 char StartOras[LG_MAX], finOras[LG_MAX];

 void Lee(poz );
 void Bordare();
 void Read();
 void Solve();
 int Valid(int, int );
 void setare();

int main()
{
    freopen("lgdrum.in", "rt", stdin);
    freopen("lgdrum.out", "wt", stdout);
    Read();
    Bordare();
    Solve();

    return 0;
}

void Read()
{
    int i, j,  p = 0, s = 0, k = 0;
    char c[LG_MAX];

    scanf("%d %d\n", &n, &m);
    scanf("%s ", StartOras);
    scanf("%s\n", finOras);

    p = strlen(finOras);

    for(i = 1; i <= n; i++)
            for(j = 1; j <= m; j++)
                {
                    scanf("%s ", c);
                    if(strcmp(c, StartOras) == 0)
                            {
                                coada[nr].x = i;
                                coada[nr++].y  = j;
                            }
                    else
                        if(strcmp(c, finOras) == 0)
                                viz[i][j] = -2;


                        p = s = 0;
                        p = strlen(c);
                        for(k = 0; k < p; k++)
                            s += c[k];
                        mat[i][j] = s;

                }
}

void Solve()
{
    int i;

    for(i = 0; i < nr; i++)
        {
            setare();
            Lee(coada[i]);
            if(lgstop && lgstop < lgMin)
                lgMin = lgstop;

            lgstop = 0;
        }
        printf("%d\n", lgMin);
}

void setare()
{
    int i, j;
    for(i = 1; i <= n; i++)
        for(j = 1; j <= m; j++)
              m1[i][j] = 0;
}

void Lee(poz start)
{
    const int dx[] = {1, -1, 0, 0};
    const int dy[] = {0, 0, 1, -1};

    int prim = 0, ultim = 0;
    poz coada[n * m];
    coada[prim] = start;
    m1[start.x][start.y] = 1;

    while(prim <= ultim)
    {
        poz pozcur = coada[prim];
        prim++;
        for(int i = 0; i < 4; i++)
        {
                poz pozurm;
                pozurm.y = pozcur.y + dy[i];
                pozurm.x = pozcur.x + dx[i];

                if(Valid(mat[pozcur.x][pozcur.y],mat[pozurm.x][pozurm.y]) && m1[pozurm.x][pozurm.y] == 0)/* && viz[pozurm.x][pozurm.y] != -1*/
                    {
                        m1[pozurm.x][pozurm.y] = m1[pozcur.x][pozcur.y] + 1;
                        ultim++;
                        coada[ultim] = pozurm;

                        if(viz[pozurm.x][pozurm.y] == -2)
                        {
                            lgstop =  m1[pozurm.x][pozurm.y];

                            return;
                        }
                    }
            }
    }

}

int Valid(int a, int b)
{
  return (a & (~b));
}

void Bordare(){

     for(int i = 0; i <= n+1; i++)
            m1[i][0] = m1[i][m+1] = -1;
     for(int i = 0; i <= m+1; i++)
            m1[0][i] = m1[n+1][i] = -1;
}

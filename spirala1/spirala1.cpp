/*
Se consideră un automat de criptare format dintr-un tablou cu n linii şi n coloane, tablou ce conţine toate numerele de la 1 la n^2 aşezate ”şerpuit”
 pe linii, de la prima la ultima linie, pe liniile impare pornind de la stânga către dreapta, iar pe cele pare de la dreapta către stânga (ca în figura alăturată).
1  -> 2  -> 3  -> 4
                  |
8  -> 7  -> 6     5
|           |     |
9     10 <- 11    12
|                 |
16 <- 15 <- 14 <-13

Numim ”amestecare“ operaţia de desfăşurare în spirală a valorilor din tablou în ordinea indicată de săgeţi şi de reaşezare a acestora în acelaşi tablou, 
”şerpuit” pe linii ca şi în cazul precedent.
De exemplu, desfăşurarea tabloului conduce la şirul: 1 2 3 4 5 12 13 14 15 16 9 8 7 6 11 10, iar reaşezarea acestuia în tablou conduce 
la obţinerea unui nou tablou reprezentat în următoarea figură.

 1  2  3  4
14 13 12  5
15 16  9  8
10 11  6  7

După orice operaţie de amestecare se poate relua procedeul, efectuând o nouă amestecare. 
S-a observat un fapt interesant: că după un număr de amestecări, unele valori ajung din nou în poziţia iniţială 
(pe care o aveau în tabloul de pornire). 
De exemplu, după două amestecări, tabloul de 4x4 conţine 9 dintre elementele sale în exact aceeaşi poziţie în 
care se aflau iniţial (vezi elemente marcate din figura următoare).

 1  2  3  4
 6  7  8  5
11 10 15 14
16  9 12 13

Cerinţă
Pentru n şi k citite, scrieţi un program care să determine numărul minim de amestecări ale unui tablou de n 
linii necesar pentru a ajunge la un tablou cu exact k elemente aflate din nou în poziţia iniţială.

Date de intrare
Fişierul de intrare spirala1.in conţine pe prima linie cele două numere n şi k despărţite printr-un spaţiu.

Date de ieşire
Fişierul de ieşire spirala1.out conţine o singură linie pe care se află numărul de amestecări determinat.

Restricţii
3<=n<=50
Datele de intrare sunt alese astfel încât numărul minim de amestecări necesare să nu depăşească 2*109.
La încheierea programului nu se va solicita apăsarea unei taste.
*/


#include <stdio.h>
#include <memory.h>
#define MAX 51
#define NMAX 2500
#define MAXL 2000000000

struct divi
{
    int care, cati;
};

struct fprim
{
    int care, ex;
};

unsigned int a[MAX][MAX], b[MAX][MAX], c[MAX][MAX];
unsigned int x[NMAX+1];

const int dx[] = {0,1,0,-1};
const int dy[] = {1,0,-1,0};
short cd, cfp;
divi dv[MAX<<2];
fprim fp[MAX<<2];
short sol[MAX<<2];
long int  cati, min, v;
int n;

void Back(int );
void Calc();
void Calc2();
void Calc3();
void Show();

int main()
{
    freopen("spirala1.in", "rt", stdin);
    freopen("spirala1.out", "wt", stdout);

    scanf("%d %ld", &n, &cati);
    Calc();
    Calc2();
    min = MAXL;
    v = 1;
    Back(1);

    if(min == MAXL)
    {
        printf("0\n");
    }
    else
    {
        printf("%ld\n",min);
    }

    return 0;
}

void Calc()
{
   long int i = 0, j = 0, k = 1, imp = 1, rez = 0, sens = 0, v[NMAX];
   ///-------initiez matricea a cu elementele initiale
   for(i = 1; i <= n; i++)
   {
       if((i&1)==1)//daca restul impartiirii la 2 este 1
       {
           for(j = 1; j <= n; j++)
           {
               a[i][j] = k++;
           }
       }
       else
       {
           for(j = n; j >= 1; j--)
           {
               a[i][j] = k++;
           }
       }
   }
   ///--------------
   ///copii matricea a in b
   memmove(b, a, sizeof(a));
 //

   do
   {
       i = 1; j = 1; sens = 0; k = 1;
       do
       {
           v[k] = a[i][j];
           a[i][j] = 0;
           if(!a[i+dx[sens]][j+dy[sens]])
           {
              sens = (sens+1)%4;
           }
           i += dx[sens];
           j += dy[sens];
          // printf("%d %d %d\n", i, j, sens);
           k++;
       }while(k <= n*n);

       ///Show();

       k = 1;
       for(i = 1; i<= n; i++)
       {
           if((i&1)==1)
           {
               for(j = 1; j <= n; j++)
               {
                   a[i][j] = v[k];
                   if(v[k] == b[i][j] && !c[i][j])
                   {
                       c[i][j] = imp;
                       rez++;
                       x[imp]++;
                   }
                   k++;
               }
           }
           else
           {
               for(j = n; j >= 1; j--)
               {
                   a[i][j] = v[k];
                   if(v[k] == b[i][j] && !c[i][j])
                   {
                       c[i][j] = imp;
                       rez++;
                       x[imp]++;

                   }
                   k++;
               }
           }
        }
        imp++;
   }while(rez != n*n);
//Show();
}

void Show()
{
    int i, j;

    for(i = 1;  i <= n; i++)
    {
        printf("\n");
        for(j = 1; j <= n; j++)
        {
            printf("%d ", a[i][j]);
        }
    }
}

void Calc2()
{
   short e;
   long int i, j, k, ex, n;

   cd = 0;
   for(i = 1; i <= NMAX; i++)
   {
      if(x[i]>0)
      {
          cd++;
          dv[cd].care = i;
          dv[cd].cati = x[i];
      }
   }
   cfp = 0;

   for(i = 2; i <= cd; i++)
   {
        k = dv[i].care;

        for(j = 1; j <= cfp; j++)
        {
            n = fp[j].care;
            ex = 0;
           while(!(k%n))
           {
               k /= n;
               ex++;
           }
           if(ex > fp[j].ex)
           {
               fp[j].ex = ex;
           }
        }
       j = 2;
       while(k > 1)
       {
           if(!(k%j))
           {
               fp[++cfp].care = j;
               fp[cfp].ex = 0;

                while(!(k%j))
                {
                    fp[cfp].ex++;
                    k /= j;
                }
           }
           j++;
       }
   }
}

void Calc3()
{
    short i = 0, j = 0, soll[MAX<<2];
    long int tot = 0, k = 0;

    for(i = 1; i <= cd; i++)
    {
        k = dv[i].care;
        memmove(soll, sol, sizeof(sol));

        for(j = 1; j <= cfp; j++)
        {
            while(soll[j] > 0 && !(k%fp[j].care))
            {
                k /= fp[j].care;
                soll[j]--;
            }
        }
        if(k == 1)
        {
            tot += dv[i].cati;
        }
    }
    if(tot == cati)
    {
        if(v < min)
        {
            min = v;
        }
    }
}

void Back(int i)
{
    int j = 0;
    long int p = 0;

    if(i > cfp)
    {
        Calc3();
    }
    else
    {
        p = 1;
        for(j = 0; j <= fp[i].ex; j++)
        {
            if(v < MAXL/p)
            {
                sol[i] = j;
                v = v*p;
                Back(i+1);
                v = v/p;
                p = p*fp[i].care;
            }
        }
        sol[i] = 0;
    }
}
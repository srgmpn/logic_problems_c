/*
Pe Gigel îl frământă în ultima vreme următoarea problemă:  câte şiruri pot fi formate din N cifre binare dintre care K cifre să aibă valoarea 1?
Deoarece această problemă este prea simplă, el adaugă următoarea condiţie: cifrele cu valoarea 1 pot fi
 plasate doar pe poziţii pare, în timp ce cifrele cu valoarea 0 pot fi plasate oriunde în şir.

Cerinţă

Scrieţi  un program care să determine răspunsul la problema lui Gigel.

Date de intrare

Fişierul de intrare kbiti.in va conţine o singură linie pe care se afla numerele naturale N şi K separate printr-un spaţiu.

Date de ieşire

Fişierul de ieşire kbiti.out va conţine o singură linie cu răspunsul dorit de Gigel.

Restricţii

    1 ≤ N ≤ 1000
    1 ≤ K ≤ N/2
    Poziţiile în şir sunt numerotate de la 1 la N.
    Pentru 30% din teste N ≤ 40
    Pentru 60% din teste N ≤ 100

*/

#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <stdlib.h>


#define LG_MAX 500 + 1
typedef int BigNumber[LG_MAX];

int n, k;



BigNumber comb1[LG_MAX], comb2[LG_MAX];

void Read();

void Solution();
void suma(BigNumber ,BigNumber ,BigNumber );
void citire(BigNumber );
void Set_big(BigNumber );
void afisare(BigNumber );


int main(){

    freopen("kbiti.in", "rt", stdin);
    freopen("kbiti.out", "wt", stdout);

    Read();
    Solution();

return 0;
}

void Read(){

    scanf("%d %d", &n, &k);

    n/=2; ++n;

}

void Solution(){

    memset(comb1[0],0,sizeof(comb1[0]));
    comb1[0][0]=1; comb1[0][1]=1;

    memset(comb2[1],0,sizeof(comb2[1]));
   	comb2[1][0]=1; comb2[1][1]=1;

    memset(comb2[0],0,sizeof(comb2[0]));

    comb2[0][0]=1; comb2[0][1]=1;



    int i, k1;
    for( i = 2; i < n; i++){

        for( k1 = 1; k1 < i; k1++)
        {
            if (i%2 == 0)
                    suma(comb2[k1],comb2[k1-1], comb1[k1]);
            else
                    suma(comb1[k1],comb1[k1-1], comb2[k1]);
        }

            if (i%2 == 0)
            {
                memset(comb1[i],0,sizeof(comb1[0]));
                comb1[i][0]=1;
                comb1[i][1]=1;
            }
            else
            {
                memset(comb2[i],0,sizeof(comb2[0]));
                comb2[i][0]=1;
                comb2[i][1]=1;
            }
    }

     if (n%2==1)
         afisare(comb1[k]);
     else
         afisare(comb2[k]);

}



void suma(BigNumber a,BigNumber b,BigNumber s)
{
    int i,cifra,t = 0,max;

    ///completam numarul cel mai mic cu zeroouri nesemnificative
    if(a[0] < b[0]) { max = b[0]; ///for(i = a[0]+1; i <= b[0]; i++) a[i] = 0;
                                    memset(a+a[0]+1,0,b[0]*sizeof(int)); }
    else            { max = a[0]; ///for(i = b[0]+1; i <= a[0]; i++) b[i] = 0;
                                    memset(b+b[0]+1,0,a[0]*sizeof(int)); }

    for(i = 1; i <= max; i++)
    {
      cifra = a[i] + b[i] + t; ///calculam noua cifra
      s[i] = cifra % 10;
      t = cifra/10;            ///calculam cifra de transport
    }

    if(t) s[i] = t; else i--;

    s[0] = i;
}


void Set_big(BigNumber a){

    memset(a,0,sizeof(a));
    a[1] = 1;
    a[0] = 1;

}

void afisare(BigNumber x)
{
     for(int i = x[0]; i >= 1; i--)
        printf("%d",x[i]);

        printf("\n");
}

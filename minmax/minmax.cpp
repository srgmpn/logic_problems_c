
/*
Gigel este elev în clasa a VII-a şi deja încearcă să compună probleme pentru fratele lui mai mic, care este în clasa a V-a. 
Astfel, astăzi Gigel îi desenează pe 10 cartonaşe cele 10 cifre.
Apoi îi spune fratelui un număr natural, iar acesta trebuie să formeze din cartonaşele primite cel mai mic, apoi cel mai mare număr natural care are suma cifrelor egală cu numărul spus de Gigel. 
De exemplu, dacă Gigel spune 28, fratele lui va lua prima dată cartonaşele cu cifrele 4, 7, 8 şi cartonaşul cu cifra 9 şi, prin alăturare, va forma numărul 4789. Pentru cel mai mare număr va lua 
cartonaşele cu cifrele 0, 1, 2, 3, 4, 5, 6 şi 7 şi va forma numărul 76543210.

Cerinţă
Deoarece nu este uşor să determini cele două numere pe care Gigel le solicită fratelui său, scrieţi un program care să determine cele două numere min şi max, dacă se cunoaşte suma cifrelor lor.

Date de intrare
Prima linie a fişierului de intrare minmax.in conţine un singur număr natural S, reprezentând suma cifrelor numerelor care trebuie determinate.

Date de ieşire
Pe prima linie a fişierului de ieşire minmax.out se vor scrie cele două numere determinate, separate printr-un spaţiu, în ordinea min max. Dacă nu există nici un 
număr natural care să aibă suma cifrelor S, se vor afişa două valori 0, separate printr-un spaţiu.

Restricţii
0 <= S <= 45
*/
#include <stdio.h>
#include <memory.h>

int n, max;
int x[10], byt[] = {1, 2, 4, 8, 16, 32, 64, 128, 256};
int Max[10];


void Read();
void Solution();
void back();
long int Gener();

int main(){

   freopen("minmax.in", "rt", stdin);
   freopen("minmax.out", "wt", stdout);

    Read();
    Solution();

return 0;
}

void Read(){

scanf("%d", &n);

}

void Solution(){

    int min = 0, max = 0;


    for(int i = 9; i >= 1; i--)
          if((min + i) <= n){min += i; x[i]++;}
    if(!min)printf("0");

    for(int i = 1; i <= 9; i++)
            if(x[i])
                    printf("%d", i);

    memset(x, 0, sizeof(x));

    printf(" ");

    back();

    Max[0]++;

    for(int i = 9; i >= 0; i--)
                            if(Max[i])
                            printf("%d", i);

}

 void back(){

     int  s = 0, l = 1;
     long int maxx = 0, k = 0;

    while(l < 512){

                memset(x, 0, sizeof(x));

                 s = 0;

                for(int i = 0; i < 9; i++)
                        if((l&byt[i])) x[i+1] = 1;


                for(int i = 1; i < 10; i++)

                            if(x[i]){

                                    s += i;


                                    }
                    l++;



                    if(s == n ){
                                  k = Gener();

                                        if(maxx < k){

                                                   maxx = k;

                                                    memcpy(Max, x, sizeof(x));}
                                          }
            }


}

long int Gener(){

    long int s = 0;

    for(int i = 9; i >= 1; i--)

            if(x[i])s = s*10 + i;

    return s;
}


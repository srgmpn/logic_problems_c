/*
Se considera sirul numerelor naturale prime:
2, 3, 5, 7, 11, 13, 17, ...

Din el se construieste un al doilea sir astfel: fiecare numar prim de pe o pozitie impara se concateneaza cu 
numarul prim urmator. Se obtine sirul:
23, 57, 1113, ...

În fine, din acest sir se formeaza un al treilea sir care contine doar numerele prime:
23, 3137, ...

Cerinta

Pentru un numar dat n, sa se obtina al n-lea numar din al treilea sir.

Date de intrare

Fisierul de intrare perechi1.in contine pe prima linie n.

Date de iesire

Fisierul de iesire perechi1.out contine pe prima linie cel de-al n-lea termen din al treilea sir.

Restrictii

    1 <= n <=100

*/
#include <stdio.h>
#include <math.h>

#define N 50000

int n, s;

int isPrime(long long int);
int Ret(int);
void Eratostene();

long int Array[N>>2], NrP;

int main()
{
   long long int x = 0, val = 0;
   long int i;

     freopen("perechi1.in","r",stdin);
    freopen("perechi1.out","w",stdout);

    scanf("%d", &n);

   Eratostene();

   i = 1;
   /// printf("%d ", isPrime(2287722901));
  while(n>0)
        {
             x = Array[i]*pow(10, Ret(Array[i+1])) + Array[i+1];
             if( !((x - 1)%6) || !((x + 1)%6))
                    if(isPrime(x))
                                {
                                 --n;
                                 val = x;

                                }
            i += 2;
        }
printf("%lld\n",val);
    return 0;
}


int isPrime(long long int n1)
{
    long int  i;
    for(i = 1; Array[i] <= sqrt(n1); i++)
        {
            if(n1%Array[i] == 0) return 0;

        }

    return 1;
}

int Ret(int n2)
{
    int i = 0;
    while(n2 > 0)
        {
            i++;
            n2 /= 10;
        }
    return i;
}


void Eratostene()
{

    long int  i, j, C, R;
    bool Prim, Term;

    Array[1] = 2;
    Array[2] = 3;

    NrP = 2;

    for(i = 4; i < N; i++)
        {
            j = 0;
            Prim = true;
            Term = false;

            while(Prim && !Term)
                {
                    j = j + 1;
                    C = i/Array[j];
                    R = i%Array[j];

                    if(!R) Prim = false;

                    if(C < Array[j])Term = true;

                }
                if(Prim)
                        {
                            Array[++NrP] = i;

                        }
        }

}

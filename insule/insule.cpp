/*
Arhipelagul RGB este format din insule care aparţin ţărilor R, G şi B. 
Putem reprezenta harta arhipelagului ca o matrice cu n linii şi m coloane cu elemente din mulţimea {0, 1, 2, 3}. 
Un element egal cu 0 reprezintă o zonă acoperită de apă; un element egal cu 1 reprezintă o zonă de pământ aparţinând unei insule din ţara R, 
iar un element egal cu 2 reprezintă o zonă de pământ aparţinând unei insule din ţara G, iar un element egal cu 3 reprezintă o zonă de pământ 
aparţinând unei insule din ţara B.
Se consideră că două elemente ale matricei sunt vecine dacă ele au aceeaşi valoare şi fie sunt consecutive pe linie, fie sunt consecutive pe coloană.
 Două elemente aparţin aceleiaşi insule dacă ele sunt vecine sau dacă se poate ajunge de la un element la celălalt pe un drum de-a lungul căruia oricare
  două elemente consecutive sunt vecine.
Pentru a încuraja relaţiile de colaborare dintre ţările R şi G, se doreşte construirea unui pod care să unească o insulă aparţinând 
ţării R de o insulă aparţinând ţării G. Podul trebuie să respecte următoarele condiţii:

    să înceapă pe o zonă cu apă consecutivă pe linie sau coloană cu o zonă aparţinând ţării R;
    să se termine pe o zonă cu apă consecutivă pe linie sau coloană cu o zonă aparţinând ţării G;
    să traverseze numai zone acoperite cu apă;
    oricare două elemente consecutive ale podului trebuie să fie vecine;
    lungimea podului să fie minimă (lungimea podului este egală cu numărul de elemente traversate de pod).

Cerinţă

Dată fiind harta arhipelagului să se determine câte insule aparţin fiecărei ţări, precum şi lungimea minimă a unui pod care să satisfacă condiţiile din enunt.

Date de intrare

Fişierul de intrare insule.in conţine pe prima linie numerele naturale n şi m, separate prin spaţiu. Pe următoarele n linii este descrisă harta arhipelagului.
Pe fiecare dintre aceste n linii sunt scrise câte m valori din mulţimea {0, 1, 2, 3}; valorile nu sunt separate prin spaţii.

Date de ieşire

Fişierul de ieşire insule.out va conţine o singură linie pe care vor fi scrise patru numere naturale separate prin spaţii NR NG NB Lg, 
unde NR reprezintă numărul de insule aparţinând ţării R, NG numărul de insule aparţinând ţării G, NB numărul de insule aparţinând ţării B,
iar Lg lungimea minimă a podului.

Restricţii

    1 < n, m ≤ 100
    Se garantează că pe hartă există cel puţin un element 1, un element 2 şi un element 0.
    Se acordă 40% din punctaj pentru determinarea corectă a numărului de insule din fiecare ţară; se acordă punctaj integral pentru rezolvarea corectă a tuturor cerinţelor.
    Începutul şi sfârşitul podului pot să coincidă.
    Pentru datele de test există întotdeauna soluţie.

*/
#include <stdio.h>
#include <memory.h>


    int m1[102][102];
    int m2[103][103];
    int m3[103][103];

   struct poz {int x, y;};

    const int dx[] = {0, -1, 0, 1};
    const int dy[] = {1, 0, -1, 0};

    poz start,maxx, InsZero[3000], InsZero2[3000];


    int n, m;
    int min = 32500 ;

    int  k1 = 0, k2 = 0, k3 = 0, Nrz;

    void Read();
    void NrInsule();
    void Insula(int , int ,int );
    void Bordare(int (*)[103], int , int );
    void Lee(int (*)[103], int , int , poz);
    void DrumMin();
    void FILL(int , poz);

int main()
{

    freopen("insule.in", "rt", stdin);
    freopen("insule.out", "wt", stdout);

    Read();

    NrInsule();

  Bordare(m3,n, m);

   DrumMin();

    return 0;
}

void Read(){


    char c;


  scanf("%d %d\n", &n, &m);

    for(int i = 1; i <= n; i++){
        for(int k = 1; k <= m; k++){
                scanf("%c", &c);

                 c = c - '0';

                m2[i][k] = c;
                m3[i][k] = -c;

        }
        scanf("\n");
    }

}

void DrumMin(){

 memmove(m2, m3, sizeof(m3));


 for(int i = 0; i < k1; i++)
        {

            FILL(-1, InsZero[i]);



    for(int j = 0; j < Nrz; j++)
                {


                Lee(m2, n, m, InsZero2[j]);

                if(min > m2[maxx.x][maxx.y] && m2[maxx.x][maxx.y] > 0)min = m2[maxx.x][maxx.y];

                                               /**  for(int  i = 1; i <= n; i++){
                                                printf("\n");
                                            for(int k = 1; k <= m; k++)
                                                printf("%3d", m2[i][k]);
                                                 ///printf("\n%d %d", InsZero2[i].x, InsZero2[i].y);
                                   }*/
                 memmove(m2, m3, sizeof(m3));
                }
       Nrz = 0;
        }



    printf("%d", min);
}

void NrInsule(){



        for(int i = 1; i <= n; i++)
            for(int k = 1; k <= m; k++)

                if(m2[i][k] == 1)
                    {

                     InsZero[k1].x = i;
                     InsZero[k1++].y = k;

                     Insula(1,i, k);

                    }

                else
                    if(m2[i][k] == 2)
                            {
                            Insula(2,i, k);
                            k2++;
                            }
                else
                    if(m2[i][k] == 3){k3++; Insula(3,i, k);}



printf("%d %d %d ",k1, k2, k3 );
}

void Insula(int a, int i,int j){

        int prim = 0, ultim = 0;

    poz start1, coada[n*m];

    start1.x = i;
    start1.y = j;

    coada[prim] = start1;

    m2[start1.x][start1.y] = -7;

    while(prim <= ultim) {

        poz pozcur = coada[prim];

        prim++;
        for(int i = 0; i < 4; i++) {

                poz pozurm;

                pozurm.y = pozcur.y + dy[i];

                pozurm.x = pozcur.x + dx[i];

                if(m2[pozurm.x][pozurm.y] == a) {

                        m2[pozurm.x][pozurm.y] = -7;
                        ultim++;
                        coada[ultim] = pozurm;}

                    }
            }
}


void FILL(int a, poz start1){

        int prim = 0, ultim = 0;

    poz coada[n*m];
    coada[prim] = start1;

    m2[start1.x][start1.y] = -7;

    while(prim <= ultim) {

        poz pozcur = coada[prim];

        prim++;
        for(int i = 0; i < 4; i++) {

                poz pozurm;
                pozurm.y = pozcur.y + dy[i];

                pozurm.x = pozcur.x + dx[i];

                if(m2[pozurm.x][pozurm.y] == a)
                    {
                        m2[pozurm.x][pozurm.y] = -7;
                        ultim++;
                        coada[ultim] = pozurm;
                    }
                else
                    if(m2[pozurm.x][pozurm.y] == 0 && !m1[pozurm.x][pozurm.y])
                            {
                                InsZero2[Nrz++] = pozurm;
                                m1[pozurm.x][pozurm.y] = 1;
                            }

        }
    }
}

void Bordare(int a[][103], int n, int m){

         for(int i = 0; i <= n+1; i++)
            a[i][0] = a[i][m+1] = -5;

        for(int j = 0; j <= m+1; j++)
             a[0][j] = a[n+1][j] = -5;
 }

void Lee(int a[][103], int n, int m, poz start)
{


    int prim = 0, ultim = 0;

    poz coada[n * m];
    coada[prim] = start;

    a[start.x][start.y] = 1;

    while( prim <= ultim){

        poz pozcur = coada[prim];

        prim++;
        for(int i = 0; i < 4; i++) {

                poz pozurm;

                pozurm.y = pozcur.y + dy[i];

                pozurm.x = pozcur.x + dx[i];

                if(a[pozurm.x][pozurm.y] == 0) {

                        a[pozurm.x][pozurm.y] = a[pozcur.x][pozcur.y] + 1;
                        ultim++;
                        coada[ultim] = pozurm;
                        }
                else
                    if(a[pozurm.x][pozurm.y] == -2){

                            maxx = pozcur;

                            return;
                    }
               /* else
                   if(a[pozurm.x][pozurm.y] > a[pozcur.x][pozcur.y]){

                            maxx1 = pozcur;
                            maxx2 = pozurm;

                    }*/
                }
        }
}

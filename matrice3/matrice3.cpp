/*
	Fie n un numar natural nenul. Vom construi o matrice cu n linii si n coloane dupa regulile ce pot fi deduse din exemplul urmator (în care n=7):

1  1  1  1  1  1  1
1  1  2  3  4  5  6
1  2  1  3  6 10 15
1  3  3  1  4 10 20
1  4  6  4  1  5 15
1  5 10 10  5  1  6
1  6 15 20 15  6  1

Cerinta

Determinati valoarea care se afla în matrice pe o pozitie specificata.

Date de intrare

Fisierul de intrare matrice.in contine pe prima linie 3 numere naturale separate prin câte un spatiu: n i j, unde n este dimensiunea matricei,
i este linia, iar j este coloana pe care se afla în matrice valoarea ce trebuie determinata.

Date de iesire

Fisierul de iesire matrice.out va contine o singura linie pe care va fi scris un singur numar natural,
reprezentând valoarea aflata în matrice pe linia i si coloana j.

Restrictii si precizari

    0<n<=500
    Liniile si respectiv coloanele matricei sunt numerotate de la 1 la n.

*/
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <stdlib.h>
#define NMAX 504

#define LG_MAX 300 + 1

typedef int BigNumber[LG_MAX];

BigNumber LinIntii[NMAX], LinDoi[NMAX];

int n, X, Y;

void Read();

void Solution();
void suma(BigNumber ,BigNumber ,BigNumber );
void citire(BigNumber );
void Set_big(BigNumber );
void afisare(BigNumber );

int main()
{
	freopen("matrice3.in", "rt", stdin);
    freopen("matrice3.out", "wt", stdout);

    Read();
    Solution();

	return 0;
}

void Read()
{

    scanf("%d %d %d", &n, &X, &Y);

}


void Solution(){

	   int i, k1, aux;

	   if(X == Y || X == 1 || Y == 1)
			   {
						printf("1");
			   }
        else
            if(X == 2)
                {
                printf("%d", Y);
                }
        else
          if(Y == 2)
            {
                printf("%d", X);
            }

	   else
			{
				if(Y > X)
				{
					aux = X;
					X = Y;
					Y = aux;

				}

				memset(LinIntii[0],0,sizeof(LinIntii[0]));
				LinIntii[0][0]=1; LinIntii[0][1]=1;

				memset(LinDoi[1],0,sizeof(LinDoi[1]));
   				LinDoi[1][0]=1; LinDoi[1][1]=1;

				memset(LinDoi[0],0,sizeof(LinDoi[0]));

				LinDoi[0][0]=1; LinDoi[0][1]=1;




			for( i = 2; i < X ; i++){

				for( k1 = 1; k1 < i ; k1++)
				{
					if (!(i&1))
							suma(LinDoi[k1],LinDoi[k1-1], LinIntii[k1]);
					else
                            suma(LinIntii[k1],LinIntii[k1-1], LinDoi[k1]);

				}

					if (!(i&1))
					{
						memset(LinIntii[i],0,sizeof(LinIntii[0]));
						LinIntii[i][0]=1;
						LinIntii[i][1]=1;


					}
					else
					{
					    //'memmove(LinDoi[i/ + 1],LinDoi[i>>1], sizeof(LinDoi[i>>1]));

						memset(LinDoi[i],0,sizeof(LinDoi[0]));
						LinDoi[i][0]=1;
						LinDoi[i][1]=1;

					}
			}

			 if ((X&1))
				 afisare(LinIntii[Y-1]);
			 else
				 afisare(LinDoi[Y-1]);
    }
}


void suma(BigNumber a,BigNumber b,BigNumber s)
{
    int i,cifra,t = 0,max;

    ///completam numarul cel mai mic cu zeroouri nesemnificative
    if(a[0] < b[0]) { max = b[0]; ///for(i = a[0]+1; i <= b[0]; i++) a[i] = 0;
                                    memset(a+a[0]+1,0,b[0]*sizeof(int)); }
    else            { max = a[0]; ///for(i = b[0]+1; i <= a[0]; i++) b[i] = 0;
                                    memset(b+b[0]+1,0,a[0]*sizeof(int)); }

    for(i = 1; i <= max; i++)
    {
      cifra = a[i] + b[i] + t; ///calculam noua cifra
      s[i] = cifra % 10;
      t = cifra/10;            ///calculam cifra de transport
    }

    if(t) s[i] = t; else i--;

    s[0] = i;
}


void Set_big(BigNumber a){

    memset(a,0,sizeof(a));
    a[1] = 1;
    a[0] = 1;

}

void afisare(BigNumber x)
{
     for(int i = x[0]; i >= 1; i--)
        printf("%d",x[i]);

        printf("\n");
}

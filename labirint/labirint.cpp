/*
Sa consideram un labirint de forma dreptunghiulara, format din n*m camere dispuse sub forma unei matrice cu n linii si m coloane. Vom considera ca liniile sunt numerotate de la 1 la n, iar coloanele sunt numerotate de la 1 la m. Camerele labirintului au usi care se deschid intr-o singura directie, permitand deplasarea in una dintre cele 8 camere invecinate. Pentru a codifica iesirile din camere, in fiecare element al matricei ce codifica labirintul va fi memorat un numar natural din intervalul [0, 255]. Fiecare bit 1 din reprezentarea binara a unui element din matrice corespunde unei usi in una dintre directiile posibile de miscare, în urmatoarea ordine: N, NE, E, SE, S, SV, V, NV. Mai exact daca bitul are valoarea 1 exista usa in directia corespunzatoare bitului, iar daca bitul are valoarea 0 usa corespunzatoare nu exista. Bitii sunt numerotati începând cu cel mai putin semnificativ.
In labirint se afla un soarece in camera de pe linia L0 si coloana C0. Soarecele ar vrea sa iasa din labirint, dar numai dupa ce a trecut printr-un anumit numar de camere (nmin). Imediat ce a trecut prin cele nmin camere soarecele poate parasi labirintul. Soarecele este istet si nu va trece de doua ori prin aceeasi camera. Soarecele poate iesi din labirint doar daca el se afla într-o camera care are usa într-o directie care îl conduce în afara labirintului.

Cerinta

Scrieti un program care sa determine un traseu al soarecelui care sa aiba cel putin lungimea nmin si care sa nu treaca de doua ori prin aceeasi camera. Traseul va incepe din camera initiala a soarecelui si se va termina intr-o camera care are usa spre iesirea din labirint.

Date de intrare

Fisierul de intrare labirint.in contine pe prima linie doua numere naturale separate printr-un spatiu n si m cu semnificatia din enunt. Pe fiecare dintre urmatoarele n linii se afla cate m numere naturale separate prin spatii, reprezentand codificarea labirintului. Pe urmatoarea linie a fisierului de intrare se afla doua numere naturale separate printr-un spatiu L0 si C0, reprezentand linia si coloana corespunzatoare camerei in care se afla initial soarecele. Ultima linie a fisierului de intrare contine numarul minim de camere prin care trebuie sa treaca soarecele soarecele înainte de a parasi labirintul.

Date de iesire

Fisierul de iesire labirint.out va contine pe prima linie un numar natural Lg, reprezentand numarul de camere pe care le poate vizita soarecele pana la iesire, fara a trece de mai multe ori prin aceeasi camera si trecand prin cel putin nmin camere. Pe urmatoarele Lg linii se afla cate doua numere naturale separate printr-un spatiu L C, reprezentand coordonatele camerelor (linie, coloana) prin care trece soarecele pe traseul determinat.

Restrictii

    0 < n, m <= 50
    Pentru datele de test problema are intotdeauna solutie.
*/
#include <stdio.h>
#include <stdlib.h>

    int m1[52][52];
    int m2[52][52];
    int m3[52][52];
    int d1[2501][2];


    struct poz {int x, y;};

    const int dy[] = { 0, 1, 1, 1, 0, -1, -1,  -1};
    const int dx[] = {-1,-1, 0, 1, 1,  1,  0,  -1};

    const int byt[] = {1, 2, 4, 8, 16, 32, 64, 128};

    int n, m, dmin, x, y, pas = 1,  lgmin = 1;


    poz start, stop;

    void Read();
    int Bord(int, int );
    void back(int, int, int);
    void Marc();
    int Drummin();
    int MArgin(int , int );


int main(){

     freopen("labirint.in","rt",stdin);
     freopen("labirint.out","wt",stdout);

    Read();



    return 0;
}

void Read(){
      scanf("%d %d",&n,&m);
      for(int j = 1; j <= n; j++)
         for(int i = 1; i <= m; i++)
                    scanf("%d ",&m2[j][i]);

       scanf("%d%d",&start.x,&start.y);

       scanf("%d",&dmin);

            x = start.x;
            y = start.y;

            d1[0][0] = x;
            d1[0][1] = y;
            Marc();
            back(x ,y, 1);
            m1[x][y] = 1;

    }

void back(int x,int y, int pas){



    if(pas >= dmin && MArgin(x, y) && m3[x][y]){


                            printf("%d\n", pas);
                            for(int i = 0; i < pas; i++)
                                    {
                                    printf("%d %d\n",d1[i][0], d1[i][1]);
                                    }

                            fclose(stdin);
                            fclose(stdout);
                            exit(0);
    }

    else{
               for(int k = 0; k < 8; k++){
                            if(!m1[x + dx[k]][y + dy[k]] && Bord( x + dx[k],y + dy[k]) && (m2[x][y] & byt[k]) == byt[k])
                                    {
                                            d1[pas][0] = x + dx[k];
                                            d1[pas][1] = y + dy[k];
                                            m1[x + dx[k]][y + dy[k]] = 1;
                                            back(x + dx[k],y + dy[k] ,pas + 1);
                                            m1[x + dx[k]][y + dy[k]] = 0;
                                    }

                            }
            }
}

int Bord(int x, int y){

    if( x < 1 || x > n || y < 1 ||y > m)return 0;

    return 1;

}

int MArgin(int x, int y){

    if( x == 1 || x == n || y  == 1 ||y == m)return 1;

    return 0;
}

void Marc(){
     ///caut punctul de iesire la vest ///est(conform conditiei)
         for(int i = 1; i <= n; i++)
         {
                    if(m2[i][1] && ((m2[i][1] & byt[5]) == byt[5] ||
                       (m2[i][1] & byt[6]) == byt[6] || (m2[i][1] & byt[7]) == byt[7] ))

                                                        m3[i][1] = 1;

                     if(m2[i][n] && ((m2[i][n] & byt[n]) == byt[1] ||
                       (m2[i][n] & byt[2]) == byt[2] || (m2[i][n] & byt[3]) == byt[3]))

                                                   m3[i][n] = 1;

         }
        ///nord  sud
        for(int i = 1; i <= m; i++)
        {
                    if(m2[1][i] && ((m2[1][i] & byt[0]) == byt[0] ||
                       (m2[1][i] & byt[7]) == byt[7] || (m2[1][i] & byt[1]) == byt[1]))

                                                   m3[1][i] = 1;

        if(m2[i][m] && ((m2[i][m] & byt[3]) == byt[3] ||
                       (m2[i][m] & byt[4]) == byt[4] || (m2[i][m] & byt[5]) == byt[5]))

                                                    m3[i][m] = 1;
        }

}
